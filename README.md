# FTP99

Reliable file transmission over UDP.
Internet and Web Engineering project.

## License
**Each and every file in this repository has to be considered covered by the GNU General Public License v3, contained in the LICENSE file.**
**Code authors are: Roberto Masocco, Francesco Mancini, Alessandro Picco.**
