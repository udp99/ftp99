/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 5/9/2019
 * -----------------------------------------------------------------------------
 * Source code for auxiliary routines of the server application of FTP99.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>
#include <signal.h>

#include "../librtl/rtl.h"

#include "../FTP99.h"

#define UNUSED(arg) (void)arg

extern AVLIntTree *clientProcs;
extern pthread_mutex_t clientsLock;
extern pthread_t reaperTID;
extern int accSock;
extern ulong clientNum;
extern DLList *workers;
extern pthread_mutex_t *workersLock;
extern RTLConnectValues *clientConn;

/* Little routine that executes "ls" for directory listings. */
int lister(void *pipePtr) {
    int pipeFD = *(int *)pipePtr;
    char *pwd = getenv("PWD");
    if (dup2(pipeFD, STDOUT_FILENO) == -1) exit(EXIT_FAILURE);
    execlp("ls", "ls", "-la", "--color=never", pwd, NULL);
    // The following code is executed only if execlp fails.
    write(pipeFD, ERR "Server fatal error.", 22);
    fprintf(stderr, "ERROR: Failed to execute ls.\n");
    perror("execlp");
    exit(EXIT_FAILURE);
}

/* Termination routine for clients reaper thread. */
void reaperCleanup(void *args) {
    UNUSED(args);
    // Avoid deadlocks.
    sigset_t ignSet;
    sigfillset(&ignSet);
    pthread_sigmask(SIG_SETMASK, &ignSet, NULL);
    int exitCd = 0;
    pthread_mutex_lock(&clientsLock);
    ulong procs = clientProcs->nodesCount;
    if (procs > 0) {
        pid_t *clientsPIDs = (pid_t *)intBFS(clientProcs, BFS_LEFT_FIRST,
                                             SEARCH_KEYS);
        for (ulong i = 0; i < procs; i++) {
            kill(clientsPIDs[i], SIGUSR1);
            waitpid(clientsPIDs[i], &exitCd, 0);
            printf("- Client worker process terminated (PID: %d).\n",
                    clientsPIDs[i]);
            if (WIFEXITED(exitCd))
                printf("-- Exit code: %d.\n", WEXITSTATUS(exitCd));
            if (WIFSIGNALED(exitCd)) {
                printf("-- Received signal: %d.\n", WTERMSIG(exitCd));
                if (WCOREDUMP(exitCd))
                    printf("-- Dumped core.\n");
            }
        }
        free(clientsPIDs);
    }
    deleteIntTree(clientProcs);
    pthread_mutex_destroy(&clientsLock);
}

/* Termination routine for the whole FTP99 Server application. */
void mainTermHandler(int sig) {
    if (sig == SIGPIPE) {
        fprintf(stderr, "\nERROR: Connection error in main process.\n");
    } else printf("\b\b  \nFTP99 Server terminating...\n");
    close(accSock);
    // Kill all clients processes by initiating reaper thread cancellation.
    pthread_cancel(reaperTID);
    pthread_join(reaperTID, NULL);
    printf("FTP99 Server shutting down...\n");
    if (sig == SIGPIPE) exit(EXIT_FAILURE);
    else exit(EXIT_SUCCESS);
}

/* Signal handler for SIGCHLD (reaper thread). */
void childCloseHandler(int sig, siginfo_t *info, void *ucontext) {
    UNUSED(sig);
    UNUSED(ucontext);
    pid_t deadChild = info->si_pid;
    int exitCd = 0;
    waitpid(deadChild, &exitCd, WNOHANG);  // Remove the zombie.
    // Log some insights about why the child terminated.
    printf("- Client worker process terminated (PID: %d).\n", deadChild);
    if (info->si_code == CLD_EXITED) {
        printf("-- Client worker process exited with code %d.\n",
                info->si_status);
    } else if (info->si_code == CLD_KILLED) {
        printf("-- Client worker process killed by signal %d with code %d"
               ".\n", WTERMSIG(exitCd), info->si_status);
    } else if (info->si_code == CLD_DUMPED) {
        printf("-- Client worker process dumped core.\n");
    }
    // Remove dead process from the index.
    pthread_mutex_lock(&clientsLock);
    if (intDelete(clientProcs, (int)deadChild) == 1) {
        printf("--- Removed from the index.\n");
    } else printf("--- Not found in the index.\n");
    pthread_mutex_unlock(&clientsLock);
}

/* Termination routine for FTP99 client process. */
void childTermHandler(int sig) {
    if (sig == SIGUSR1)
        printf("Client worker no. %lu closing...\n", clientNum);
    else if (sig == SIGPIPE)
        fprintf(stderr, "ERROR: Connection error on client no. %lu.\n",
                clientNum);
    // Terminate other workers and close the connection on this end.
    // Block all signals now to avoid deadlocks.
    sigset_t ignSet; sigfillset(&ignSet);
    pthread_sigmask(SIG_SETMASK, &ignSet, NULL);
    pthread_mutex_lock(workersLock);
    if (workers->recsCount > 0) {
        Record *worker = workers->first;
        while (worker != NULL) {
            pthread_t workerTID = ((__workerArgs *)(worker->recData))
                                   ->workerTID;
            pthread_cancel(workerTID);
            worker = worker->next;
            popFirst(workers);
        }
    }
    pthread_mutex_destroy(workersLock);
    eraseList(workers);
    rtlClose(clientConn);
    // Terminate this process.
    if (sig == SIGUSR1) {
        printf("Client worker no. %lu terminated!\n", clientNum);
        exit(EXIT_SUCCESS);
    } else if (sig == SIGPIPE) {
        fprintf(stderr, "Client worker no. %lu exiting...\n", clientNum);
        exit(EXIT_FAILURE);
    }
}
