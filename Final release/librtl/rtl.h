/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 19/8/2019
 * -----------------------------------------------------------------------------
 * Header file for the RTL Library.
 * Include this.
 */

#ifndef _RTL_H
#define _RTL_H

#include <sys/types.h>

#include "librtl_definitions.h"

/* RTL control APIs. */
int rtlInit(const char *ip_addr, ushort port);
RTLConnectValues *rtlConnect(const char *srv_addr, ushort port, int sockFD,
        unsigned int N, int T, unsigned char p);
RTLAcceptValues *rtlAccept(int sock);
RTLConnectValues *rtlAttach(RTLAcceptValues *accData, int oldSock, int forking);
int rtlClose(RTLConnectValues *connData);

/* RTL communication APIs. */
int rtlOpenChannel(RTLConnectValues *connData , int newChan);
int rtlCloseChannel(int channel, int chanReadFD, RTLConnectValues *connData);
ssize_t rtlSend(int chan, RTLConnectValues *connData, const char *buf,
        size_t len);
ssize_t rtlRecv(int chan, int chanReadFD, RTLConnectValues *connData, char *buf,
        size_t len);

#endif
