/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 19/8/2019
 * -----------------------------------------------------------------------------
 * Source file for the RTL Library with code for internal routines and threads.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/timerfd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/socket.h>
#include <time.h>
#include <semaphore.h>
#include <netinet/in.h>

#include "rtl.h"

#include "../Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h"

/* SR sendto wrapper. */
ssize_t srSendto(int sockFD, char *buf, size_t len, unsigned char lossProb,
                 struct drand48_data *randData) {
    if (lossProb == 0)
        // Really send the packet.
        return send(sockFD, buf, len, 0);
    // Probability check. Constants and computations ensure that random
    // numbers between 0 and 100 are generated with uniform distribution.
    long int randNum = 0;
    lrand48_r(randData, &randNum);
    while (randNum >= 2147483614)
        lrand48_r(randData, &randNum);
    unsigned int chance = randNum / 21262214;
    if (chance <= (unsigned int)lossProb) return 1;
    // Really send the packet.
    return send(sockFD, buf, len, 0);
}

/* Routine to compute timeout for new packet. */
uint64_t _computeTimeout(uint64_t lastRTT, uint64_t *estRTT, uint64_t *devRTT) {
    // TCP-like adaptive algorithm.
    // Step 1: Update RTT estimation.
    *estRTT = (lastRTT >> 3) + (*estRTT - ((*estRTT) >> 3));
    // Step 2: Update RTT deviance.
    int64_t sampleRTT = (int64_t)lastRTT;
    int64_t sEstRTT = (int64_t)(*estRTT);
    int64_t devDiff = sampleRTT - sEstRTT;
    if (devDiff < 0) devDiff = -devDiff;  // Need absolute value.
    *devRTT = (devDiff >> 2) + (*devRTT - ((*devRTT) >> 2));
    // Step 3: Finally, compute timeout value.
    uint64_t newTOut = *estRTT + ((*devRTT) << 2);
    // Limit its maximum value.
    if (newTOut > __MAX_PCKT_TIMEO) newTOut = __MAX_PCKT_TIMEO;
    return newTOut;
}

/* Threads termination routines. */
void _outEntCleanup(void *args) {
    _outputEntArgs *outArgs = (_outputEntArgs *)args;
    eraseFibHeap(outArgs->_timeHeap);
    free(outArgs->_sendWnd);
    sem_destroy(outArgs->outputPipeSem);
    free(outArgs->outputPipeSem);
    close(outArgs->inputPipeFD_read);
    close(outArgs->outputPipeFD_read);
    close(outArgs->timerFD);
    close(outArgs->sockFD);  // This closes the underlying UDP socket.
    free(outArgs);
}
void _inEntCleanup(void *args) {
    _inputEntArgs *inArgs = (_inputEntArgs *)args;
    // Cancel Channel Demux.
    pthread_cancel(inArgs->chanDmxTID);
    pthread_join(inArgs->chanDmxTID, NULL);
    // Perform your own cleanup.
    close(inArgs->chanDmxPipe_write);
    close(inArgs->inputPipeFD_write);
    free(inArgs->_recvWnd);
    free(inArgs);
}
void _chanDmxCleanup(void *args) {
    _chanDmxArgs *cDArgs = (_chanDmxArgs *)args;
    close(cDArgs->chanDmxPipe_read);
    pthread_mutex_destroy(cDArgs->_chanAVLLock);
    free(cDArgs->_chanAVLLock);
    ulong wrEndsCnt = cDArgs->_chanAVL->nodesCount;
    if (wrEndsCnt > 0) {  // Chan. 0 should always be here.
        int *writeEnds = (int *)intBFS(cDArgs->_chanAVL, BFS_LEFT_FIRST,
                                       SEARCH_DATA);
        for (ulong i = 0; i < wrEndsCnt; i++)
            close(writeEnds[i]);
        free(writeEnds);
    }
    deleteIntTree(cDArgs->_chanAVL);
    free(cDArgs);
}

/* Demultiplexer entity for different transmissions of a single client. */
void *_chanDmx(void *args) {
    // Block ALL signals (leave them to Application Level).
    {
        sigset_t ignSet;
        sigfillset(&ignSet);
        pthread_sigmask(SIG_SETMASK, &ignSet, NULL);
    }

    // Name yourself.
    pthread_setname_np(pthread_self(), "RTL_chanDmx");

    // Load arguments.
    _chanDmxArgs *chanDmxArgs = (_chanDmxArgs *)args;

    // Sanity check
    if ((chanDmxArgs == NULL) || (chanDmxArgs->_chanAVL == NULL) ||
        (chanDmxArgs->_chanAVLLock == NULL) ||
        (chanDmxArgs->chanDmxPipe_read == 0))
        kill(getpid(), SIGPIPE);
    pthread_cleanup_push(_chanDmxCleanup, chanDmxArgs);
    // Variables initialization.
    // Buffer to save packets coming from lower level.
    char buffer[UDP_MAXPLD];
    memset(buffer, 0, UDP_MAXPLD);

    int chPipe = 0;
    unsigned char channel = 0;
    uint16_t dataLength = 0;
    ssize_t dataSent = 0;
    size_t dataCount = 0;
    // Copy data into the stack in order to reduce memory access.
    int readPipe = chanDmxArgs->chanDmxPipe_read;
    pthread_mutex_t *chanAVLLock = chanDmxArgs->_chanAVLLock;
    AVLIntTree *chanAVL = chanDmxArgs->_chanAVL;

    for (;;) {
        // Wait for data coming from lower level.
        if (read(readPipe, buffer, UDP_MAXPLD) == -1)
            kill(getpid(), SIGPIPE);

        // Obtain lock in order to find pipe associated to received channel.
        pthread_mutex_lock(chanAVLLock);

        // Load metadata from packet.
        // Load channel.
        channel = buffer[CHN_OFFST];
        // Load data length.
        memcpy(&dataLength, buffer + DATALEN_OFFST, 2);
        // Convert in host order.
        dataLength = ntohs(dataLength);

        // Look for pipe associated to received channel.
        if ((chPipe = intSearchData(chanAVL, (int)channel)) == -1) {
            // No channel found.
            // Clear buffer.
            memset(buffer, 0, UDP_MAXPLD);
            // Unlock AVLLock.
            pthread_mutex_unlock(chanAVLLock);
            // Re-initialize variables.
            channel = 0;
            dataLength = 0;
            continue;
        }

        // Unlock AVLLock.
        pthread_mutex_unlock(chanAVLLock);

        // Loop to send all data on the pipe.
        do {
            dataSent = write(chPipe, buffer + RTL_HEAD_LEN + dataCount,
                             (size_t)dataLength - dataCount);
            if (dataSent == -1)
                // Error in the channel.
                kill(getpid(), SIGPIPE);
            dataCount += dataSent;
        } while (dataCount != dataLength);

        // Clear buffer.
        memset(buffer, 0, UDP_MAXPLD);
        // Re-initialize variables
        channel = 0;
        dataLength = 0;
        dataSent = 0;
        dataCount = 0;
    }

    pthread_cleanup_pop(0);
}

/* Selective Repeat receiver. */
void *_inputEntity(void *args) {
    // Block ALL signals (leave them to Application Level).
    {
        sigset_t ignSet;
        sigfillset(&ignSet);
        pthread_sigmask(SIG_SETMASK, &ignSet, NULL);
    }

    // Name yourself.
    pthread_setname_np(pthread_self(), "RTL_inputEnt");

    // Load args.
    _inputEntArgs *inputEntArgs = (_inputEntArgs *)args;

    pthread_cleanup_push(_inEntCleanup, inputEntArgs);

    // Sanity check.
    if (!(inputEntArgs->sockFD) || (inputEntArgs->_recvWnd == NULL) ||
    !(inputEntArgs->chanDmxPipe_write) || !(inputEntArgs->chanDmxTID) ||
    !(inputEntArgs->inputPipeFD_write) || !(inputEntArgs->windowSize))
        kill(getpid(), SIGPIPE);
    // Variable initialization.
    // Copy data into the stack in order to decrease memory access.
    int sockFD = inputEntArgs->sockFD;

    int inputPipeFD_write = inputEntArgs->inputPipeFD_write;
    int chanDmxPipe_write = inputEntArgs->chanDmxPipe_write;

    __receiver_wnd_entry *recvWind = inputEntArgs->_recvWnd;
    uint32_t recvBase = inputEntArgs->nextInNum;
    uint32_t windowSize = inputEntArgs->windowSize;
    uint32_t maxSeqNum = inputEntArgs->maxSeqNum;

    struct drand48_data myRandData;
    memset(&myRandData, 0, sizeof(struct drand48_data));
    srand48_r(getpid(), &myRandData);

    // Buffer to save packets coming from lower level.
    char buffer[UDP_MAXPLD];
    char ack[RTL_HEAD_LEN];

    memset(buffer, 0, UDP_MAXPLD);
    memset(ack, 0, RTL_HEAD_LEN);

    unsigned char flags = 0;
    uint32_t recvSeqNum = 0;

    for (;;) {
        // Read packets from socket.
        if (recv(sockFD, buffer, UDP_MAXPLD, 0) == -1)
            kill(getpid(), SIGPIPE);

        // Load packet header.
        flags = buffer[0];
        memcpy(&recvSeqNum, buffer + SEQNUM_OFFST, 4);

        // Convert in host order.
        recvSeqNum = ntohl(recvSeqNum);

        if (flags == ACK) {
            // ACK. Send seqNum to output entity.
            if (write(inputPipeFD_write, &recvSeqNum, 4) == -1)
                kill(getpid(), SIGPIPE);
        } else {
            // Check if is the received sequence number is compliant with the
            // Selective Repeat protocol.

            int isLowerWind = 0;
            int isInsideWind = 0;

            if (recvBase == 0)
               isLowerWind = (((maxSeqNum - windowSize) <= recvSeqNum) &&
                              (recvSeqNum <= maxSeqNum));
            else if (recvBase < windowSize)
               isLowerWind =  (recvSeqNum <= (recvBase - 1)) ||
                              ((maxSeqNum - (windowSize - recvBase - 1) <=
                               recvSeqNum) && (recvSeqNum <= maxSeqNum));
            else isLowerWind = (((recvBase - windowSize) <= recvSeqNum) &&
                                ((recvBase - 1) >= recvSeqNum));

            if (recvBase > (maxSeqNum - windowSize + 1))
                isInsideWind = (((recvBase <= recvSeqNum) &&
                                 (recvSeqNum <= maxSeqNum)) ||
                                ((recvBase - recvSeqNum - 1) >=
                                ((maxSeqNum - windowSize) + 1)));
            else isInsideWind = ((recvSeqNum >= recvBase) &&
                                 (recvSeqNum <= (recvBase + windowSize - 1)));

            if (isLowerWind) {
                // Old packet, send ACK.
                recvSeqNum = htonl(recvSeqNum);

                ack[0] = ACK;
                memcpy(ack + SEQNUM_OFFST, &recvSeqNum, 4);

                send(sockFD, ack, RTL_HEAD_LEN, 0);
            } else if (isInsideWind) {
                if (flags == FIN) {
                    // Start closing connection on this side if the closing
                    // procedure hasn't been started yet here.
                    if (pthread_mutex_trylock(inputEntArgs->_closeLock) == 0) {
                        // Irreversibly lock output pipe.
                        pthread_mutex_lock(inputEntArgs->outputPipeLock);
                        // Send your FIN packet.
                        char fin[RTL_HEAD_LEN];
                        memset(fin, 0, RTL_HEAD_LEN);
                        fin[0] = FIN;
                        if (write(inputEntArgs->outputPipeFD_write, fin,
                                  RTL_HEAD_LEN) == -1)
                            kill(getpid(), SIGPIPE);
                        sem_wait(inputEntArgs->_outputPipeSem);
                        // Now the Input Entity waits to be canceled,
                        // but continues to deliver ACKs if need be.
                    }
                } else if (flags == 0) {
                    // Data packet.
                    // Check if it has already been received.
                    if (recvWind[recvSeqNum % windowSize].__receivedPkt == 0) {
                        // Copy the packet in recvWind buffer.
                        memcpy(recvWind[recvSeqNum % windowSize].__pckt,
                               buffer, UDP_MAXPLD);
                        // Set ACK flag.
                        recvWind[recvSeqNum % windowSize].__receivedPkt = 1;
                    }


                    // Send ACK to sender.
                    recvSeqNum = htonl(recvSeqNum);

                    ack[0] = ACK;
                    memcpy(ack + SEQNUM_OFFST, &recvSeqNum, 4);

                    send(sockFD, ack, RTL_HEAD_LEN, 0);


                    // Send packets to chnDMX if possible.
                    while (recvWind[recvBase % windowSize].__receivedPkt == 1) {
                        // Send packet to chanDMX.
                        if (write(chanDmxPipe_write,
                                  recvWind[recvBase % windowSize].__pckt,
                                  UDP_MAXPLD) == -1)
                            kill(getpid(), SIGPIPE);

                        // Invalidate window entry.
                        recvWind[recvBase % windowSize].__receivedPkt = 0;
                        memset(recvWind[recvBase % windowSize].__pckt, 0,
                               UDP_MAXPLD);

                        // Move window.
                        if (recvBase == maxSeqNum) recvBase = 0;
                        else recvBase++;
                    }
                }
            }
        }

        // Clean buffers.
        flags = 0;
        memset(buffer, 0, UDP_MAXPLD);
        memset(ack, 0, RTL_HEAD_LEN);
    }

    pthread_cleanup_pop(0);
}

/* Selective Repeat sender. */
void *_outputEntity(void *args) {
    // Block ALL signals (leave them to Application Level).
    {
        sigset_t ignSet;
        sigfillset(&ignSet);
        pthread_sigmask(SIG_SETMASK, &ignSet, NULL);
    }
    // Name yourself.
    pthread_setname_np(pthread_self(), "RTL_outputEnt");
    // Copy input arguments to stack and initialize packet buffer.
    _outputEntArgs *myArgs = (_outputEntArgs *)args;
    pthread_cleanup_push(_outEntCleanup, myArgs);
    int udpSock = myArgs->sockFD;
    int tOut = myArgs->tOut;
    unsigned char lossProb = myArgs->lossProb;
    uint32_t wndSize = myArgs->windowSize;
    __sender_wnd_entry *sendWnd = myArgs->_sendWnd;
    FibHeap *timeHeap = myArgs->_timeHeap;
    uint32_t outSeqNum = myArgs->firstOutNum;
    uint32_t sendBase = outSeqNum;
    uint32_t maxSeqNum = myArgs->maxSeqNum;
    int inputPipe = myArgs->inputPipeFD_read;
    int outputPipe = myArgs->outputPipeFD_read;
    int outTimer = myArgs->timerFD;
    sem_t *outputPipeSem = myArgs->outputPipeSem;
    char newPckt[UDP_MAXPLD];
    uint64_t lastRTT = 0;
    uint64_t estRTT = __INITIAL_RTT;
    uint64_t devRTT = __INITIAL_DEVRTT;
    unsigned int retrAttempts = 0;
    struct drand48_data myRandData;
    memset(&myRandData, 0, sizeof(struct drand48_data));
    srand48_r(getpid(), &myRandData);
    // Create FD sets for select.
    fd_set eventsSet;
    int activeFDs;
    int maxFD = -1;
    {
        // Don't want this stuff polluting the stack when done.
        int fds[3] = {inputPipe, outputPipe, outTimer};
        for (int i = 0; i < 3; i++)
            if (fds[i] > maxFD) maxFD = fds[i];
    }
    maxFD++;
    // Set up structures to manage timer.
    struct itimerspec disarm, timeout;
    disarm.it_value.tv_sec = 0;
    disarm.it_value.tv_nsec = 0;
    disarm.it_interval.tv_sec = 0;
    disarm.it_interval.tv_nsec = 0;
    timeout.it_interval.tv_sec = 0;
    timeout.it_interval.tv_nsec = 0;
    // Selective Repeat sender loop.
    for (;;) {
        // Reset FDs set.
        FD_ZERO(&eventsSet);            // Clear set.
        FD_SET(inputPipe, &eventsSet);  // Add input pipe for ACKs.
        FD_SET(outTimer, &eventsSet);   // Add output timer.
        // Add pipe to upper level only if there's enough space left.
        int isInsideWnd = 0;
        if (sendBase > (maxSeqNum - wndSize + 1))
            isInsideWnd = (((sendBase <= outSeqNum) &&
                            (outSeqNum <= maxSeqNum)) ||
                           ((sendBase - outSeqNum - 1) >=
                           ((maxSeqNum - wndSize) + 1)));
        else isInsideWnd = ((outSeqNum >= sendBase) &&
                            (outSeqNum <= (sendBase + wndSize - 1)));
        if (isInsideWnd)
            FD_SET(outputPipe, &eventsSet);

        // Look for incoming events.
        activeFDs = select(maxFD, &eventsSet, NULL, NULL, NULL);
        if (activeFDs == -1) kill(getpid(), SIGPIPE);   // Shit happened.
        if (FD_ISSET(inputPipe, &eventsSet)) {
            // New ACK.
            uint32_t ackNum = 0;
            if (read(inputPipe, &ackNum, 4) == -1)
                kill(getpid(), SIGPIPE);  // Shit happened.
            // Check ACK number.
            // Control numbers AND check for duplicate ACK.
            if ( (( (sendBase < outSeqNum) && ((ackNum >= sendBase) &&
                                               (ackNum < outSeqNum)) ) ||
                    ( (outSeqNum < sendBase) && ((ackNum < outSeqNum) ||
                                                 ((ackNum >= sendBase) &&
                                                  (ackNum <= maxSeqNum))) )) &&
                 (sendWnd[ackNum % wndSize].__ackd == 0)) {
                // Update RTT sample (if required, not on retransmissions).
                if ((tOut == 0) &&
                    (sendWnd[ackNum % wndSize].__transmTime != 0)) {
                    struct timespec now;
                    memset(&now, 0, sizeof(now));
                    if (clock_gettime(CLOCK_MONOTONIC, &now) == -1)
                        kill(getpid(), SIGPIPE);  // Shit happened.
                    uint64_t nowTime = (now.tv_sec * 1000) +
                                       (now.tv_nsec / 1000000);
                    lastRTT = nowTime - sendWnd[ackNum % wndSize].__transmTime;
                }
                // Zero retransmissions counter.
                retrAttempts = 0;
                // Invalidate entry in the window.
                FibTreeNode *ackedNode = sendWnd[ackNum % wndSize].__timeNode;
                memset(sendWnd[ackNum % wndSize].__pckt, 0, UDP_MAXPLD);
                sendWnd[ackNum % wndSize].__ackd = 1;
                sendWnd[ackNum % wndSize].__transmTime = 0;
                sendWnd[ackNum % wndSize].__tOutInterval = 0;
                sendWnd[ackNum % wndSize].__retrCount = 0;
                if (ackedNode == timeHeap->min) {
                    // Stop timer.
                    if (timerfd_settime(outTimer, TFD_TIMER_ABSTIME,
                            &disarm, NULL) == -1)
                        kill(getpid(), SIGPIPE);  // Shit happened.
                    FD_CLR(outTimer, &eventsSet);  // Minimum was ack'd.
                    // Remove min node from the heap.
                    eraseFibTreeNode(fhDeleteMin(timeHeap));
                    if (!isHeapEmpty(timeHeap)) {
                        // Get current time.
                        struct timespec now;
                        memset(&now, 0, sizeof(now));
                        if (clock_gettime(CLOCK_MONOTONIC, &now) == -1)
                            kill(getpid(), SIGPIPE);  // Shit happened.
                        uint64_t nowTime = (now.tv_sec * 1000) +
                                           (now.tv_nsec / 1000000);
                        FibTreeNode *newMin = timeHeap->min;
                        while (newMin->key <= nowTime) {
                            // Retransmit current min.
                            FibTreeNode *currMin = timeHeap->min;
                            uint16_t dataLen = 0;
                            memcpy(&dataLen,
                                   sendWnd[currMin->elem % wndSize]
                                   .__pckt + DATALEN_OFFST,
                                   2);
                            dataLen = ntohs(dataLen);
                            if (srSendto(udpSock,
                                         sendWnd[currMin->elem %
                                                 wndSize].__pckt,
                                         RTL_HEAD_LEN + dataLen,
                                         lossProb,
                                         &myRandData) == -1)
                                kill(getpid(), SIGPIPE);  // Shit happened.
                            sendWnd[currMin->elem % wndSize].__transmTime = 0;
                            sendWnd[currMin->elem % wndSize].__retrCount++;
                            // Compute new packet timeout.
                            uint64_t newTOut = 0;
                            if (tOut > 0) newTOut = tOut;
                            else newTOut = 2 * sendWnd[currMin->elem %
                                                       wndSize].__tOutInterval;
                            sendWnd[currMin->elem % wndSize].__tOutInterval =
                                    newTOut;
                            // Eventually reset RTT estimations.
                            if (sendWnd[currMin->elem % wndSize].__retrCount
                                == __MAX_PCKT_RETRS) {
                                sendWnd[currMin->elem % wndSize].__retrCount
                                = 0;
                                estRTT = __INITIAL_RTT;
                                devRTT = __INITIAL_DEVRTT;
                            }
                            // Increase min node key.
                            fhIncreaseKey(timeHeap, currMin,
                                          nowTime + newTOut - currMin->key);
                            // Go on.
                            newMin = timeHeap->min;
                        }
                        // Restart timer with new incoming timeout.
                        timeout.it_value.tv_sec = newMin->key / 1000;
                        timeout.it_value.tv_nsec = (newMin->key -
                                (timeout.it_value.tv_sec * 1000)) * 1000000;
                        if (timerfd_settime(outTimer, TFD_TIMER_ABSTIME,
                                            &timeout, NULL) == -1)
                            kill(getpid(), SIGPIPE);  // Shit happened.
                    }
                } else {
                    // This is some other packet in the window. Just delete it.
                    eraseFibTreeNode(fhDelete(timeHeap, ackedNode));
                }
                // NOW we can invalidate the node pointer.
                sendWnd[ackNum % wndSize].__timeNode = NULL;
                // Update window base number if oldest packet was ack'd.
                while (sendWnd[sendBase % wndSize].__ackd == 1) {
                    sendWnd[sendBase % wndSize].__ackd = 0;
                    if (sendBase == maxSeqNum) {
                        // Correctly "overflow" sequence numbers.
                        sendBase = 0;
                    } else sendBase++;
                }
                // Check if closing procedure must be initiated.
                if (sendWnd[sendBase % wndSize].__pckt[0] == FIN) {
                    char *fin = sendWnd[sendBase % wndSize].__pckt;
                    struct timespec sleep;
                    sleep.tv_sec = __FIN_TIMEO_SECS;
                    sleep.tv_nsec = __FIN_TIMEO_NANOSECS;
                    // Transmit FIN packets for some time.
                    for (int i = 0; i < __FIN_RETRS; i++) {
                        srSendto(udpSock, fin, RTL_HEAD_LEN, lossProb,
                                 &myRandData);
                        nanosleep(&sleep, NULL);
                    }
                    // Cancel Input Entity.
                    pthread_cancel(myArgs->_inputEntTID);
                    // Disable further sends.
                    sem_post(myArgs->_ioCloseSem);
                    // Perform cleanup and terminate.
                    pthread_exit(NULL);
                }
            }
        }
        if (FD_ISSET(outTimer, &eventsSet)) {
            // Packet timed out.
            // Check retransmissions counter.
            if ((retrAttempts == __MAX_RETR_ATTEMPTS) && (__RETR_ENABLE != 0))
                kill(getpid(), SIGPIPE);  // Connection is assumed lost.
            // Retransmit packet.
            uint32_t tOutSeqNum = fhFindMin(timeHeap);
            uint16_t dataLen = 0;
            memcpy(&dataLen,
                   sendWnd[tOutSeqNum % wndSize].__pckt + DATALEN_OFFST,
                   2);
            dataLen = ntohs(dataLen);
            if (srSendto(udpSock, sendWnd[tOutSeqNum % wndSize].__pckt,
                         RTL_HEAD_LEN + dataLen, lossProb, &myRandData) == -1)
                kill(getpid(), SIGPIPE);  // Shit happened.
            retrAttempts++;
            sendWnd[tOutSeqNum % wndSize].__transmTime = 0;
            sendWnd[tOutSeqNum % wndSize].__retrCount++;
            // Get current time and new timeout and increase node's key.
            struct timespec now;
            memset(&now, 0, sizeof(now));
            if (clock_gettime(CLOCK_MONOTONIC, &now) == -1)
                kill(getpid(), SIGPIPE);  // Shit happened.
            uint64_t nowTime = (now.tv_sec * 1000) +
                               (now.tv_nsec / 1000000);
            uint64_t newTimeOut = 0;
            if (tOut > 0) newTimeOut = tOut;
            else newTimeOut = 2 * sendWnd[tOutSeqNum % wndSize].__tOutInterval;
            sendWnd[tOutSeqNum % wndSize].__tOutInterval = newTimeOut;
            fhIncreaseKey(timeHeap, timeHeap->min, nowTime + newTimeOut -
                                                   (timeHeap->min->key));
            // Eventually reset RTT estimations.
            if (sendWnd[tOutSeqNum % wndSize].__retrCount == __MAX_PCKT_RETRS) {
                sendWnd[tOutSeqNum % wndSize].__retrCount = 0;
                estRTT = __INITIAL_RTT;
                devRTT = __INITIAL_DEVRTT;
            }

            FibTreeNode *newMin = timeHeap->min;
            while (newMin->key <= nowTime) {
                // Retransmit current min.
                FibTreeNode *currMin = timeHeap->min;
                uint16_t oldDataLen = 0;
                memcpy(&oldDataLen,
                       sendWnd[currMin->elem % wndSize].__pckt + DATALEN_OFFST,
                       2);
                oldDataLen = ntohs(oldDataLen);
                if (srSendto(udpSock,
                        sendWnd[currMin->elem % wndSize].__pckt,
                             RTL_HEAD_LEN + oldDataLen,
                        lossProb,
                        &myRandData) == -1)
                    kill(getpid(), SIGPIPE);  // Shit happened.
                sendWnd[currMin->elem % wndSize].__transmTime = 0;
                sendWnd[currMin->elem % wndSize].__retrCount++;
                // Compute new packet timeout.
                uint64_t newTOut = 0;
                if (tOut > 0) newTOut = tOut;
                else newTOut =
                        2 * sendWnd[currMin->elem % wndSize].__tOutInterval;
                sendWnd[currMin->elem % wndSize].__tOutInterval = newTimeOut;
                // Eventually reset RTT estimations.
                if (sendWnd[currMin->elem % wndSize].__retrCount ==
                __MAX_PCKT_RETRS) {
                    sendWnd[currMin->elem % wndSize].__retrCount = 0;
                    estRTT = __INITIAL_RTT;
                    devRTT = __INITIAL_DEVRTT;
                }
                // Increase min node key.
                fhIncreaseKey(timeHeap, currMin, nowTime + newTOut -
                                                 currMin->key);
                // Go on.
                newMin = timeHeap->min;
            }
            // Restart timer with new incoming timeout.
            timeout.it_value.tv_sec = newMin->key / 1000;
            timeout.it_value.tv_nsec = (newMin->key -
                    (timeout.it_value.tv_sec * 1000)) * 1000000;
            if (timerfd_settime(outTimer, TFD_TIMER_ABSTIME, &timeout, NULL) ==
                -1) kill(getpid(), SIGPIPE);  // Shit happened.
        }
        if (FD_ISSET(outputPipe, &eventsSet)) {
            // New data to send from upper layer.
            // WARNING: This happens only if the window isn't full.
            // Otherwise, calling threads enter a wait state until
            // enough space is freed.
            // WARNING: We assume that data coming from upper level is already
            // correctly packetized, and packets miss only the sequence number.
            // Read new packet and give feedback to upper level.
            memset(newPckt, 0, UDP_MAXPLD);
            if (read(outputPipe, newPckt, UDP_MAXPLD) == -1)
                kill(getpid(), SIGPIPE);
            sem_post(outputPipeSem);
            // Add sequence number to packet.
            uint32_t newSeqNum = htonl(outSeqNum);
            memcpy(newPckt + SEQNUM_OFFST, &newSeqNum, 4);
            // Check if it's a disconnection notification.
            if (newPckt[0] == FIN) {
                // Just add the packet to the window.
                sendWnd[outSeqNum % wndSize].__ackd = 0;
                sendWnd[outSeqNum % wndSize].__timeNode = NULL;
                sendWnd[outSeqNum % wndSize].__transmTime = 0;
                sendWnd[outSeqNum % wndSize].__tOutInterval = 0;
                sendWnd[outSeqNum % wndSize].__retrCount = 0;
                memcpy(sendWnd[outSeqNum % wndSize].__pckt,
                       newPckt, UDP_MAXPLD);
                // If the window is empty, start closing by sending the FIN.
                if (outSeqNum == sendBase) {
                    char *fin = sendWnd[sendBase % wndSize].__pckt;
                    struct timespec sleep;
                    sleep.tv_sec = __FIN_TIMEO_SECS;
                    sleep.tv_nsec = __FIN_TIMEO_NANOSECS;
                    // Transmit FIN packets for some time.
                    for (int i = 0; i < __FIN_RETRS; i++) {
                        srSendto(udpSock, fin, RTL_HEAD_LEN, lossProb,
                                 &myRandData);
                        nanosleep(&sleep, NULL);
                    }
                    // Cancel Input Entity.
                    pthread_cancel(myArgs->_inputEntTID);
                    // Disable further sends.
                    sem_post(myArgs->_ioCloseSem);
                    // Perform cleanup and terminate.
                    pthread_exit(NULL);
                }
                // Otherwise, update sequence number.
                if (outSeqNum == maxSeqNum) {
                    // Correctly "overflow" sequence numbers.
                    outSeqNum = 0;
                } else outSeqNum++;
                continue;
            }
            // Get current time and new timeout.
            struct timespec now;
            memset(&now, 0, sizeof(now));
            if (clock_gettime(CLOCK_MONOTONIC, &now) == -1)
                kill(getpid(), SIGPIPE);  // Shit happened.
            uint64_t nowTime = (now.tv_sec * 1000) +
                               (now.tv_nsec / 1000000);
            uint64_t newTimeOut = 0;
            if (tOut > 0) newTimeOut = (uint64_t)tOut;
            else newTimeOut = _computeTimeout(lastRTT, &estRTT, &devRTT);
            uint64_t newKey = nowTime + newTimeOut;
            // Transmit packet.
            uint16_t dataLen = 0;
            memcpy(&dataLen, newPckt + DATALEN_OFFST, 2);
            dataLen = ntohs(dataLen);
            if (srSendto(udpSock,
                         newPckt,
                         RTL_HEAD_LEN + dataLen,
                         lossProb,
                         &myRandData) == -1)
                kill(getpid(), SIGPIPE); // Shit happened.
            // Add packet to sender window.
            sendWnd[outSeqNum % wndSize].__ackd = 0;
            sendWnd[outSeqNum % wndSize].__transmTime = nowTime;
            sendWnd[outSeqNum % wndSize].__tOutInterval = newTimeOut;
            sendWnd[outSeqNum % wndSize].__retrCount = 0;
            memcpy(sendWnd[outSeqNum % wndSize].__pckt, newPckt, UDP_MAXPLD);
            FibTreeNode *newNode = fhInsert(timeHeap, outSeqNum, newKey);
            if (newNode == NULL) kill(getpid(), SIGPIPE);  // Shit happened.
            sendWnd[outSeqNum % wndSize].__timeNode = newNode;
            if (timeHeap->min == newNode) {
                // Stop current timer.
                if (timerfd_settime(outTimer, TFD_TIMER_ABSTIME, &disarm,
                        NULL) == -1)
                    kill(getpid(), SIGPIPE);
                // Start new timer.
                timeout.it_value.tv_sec = newKey / 1000;
                timeout.it_value.tv_nsec = (newKey -
                        (timeout.it_value.tv_sec * 1000)) * 1000000;
                if (timerfd_settime(outTimer, TFD_TIMER_ABSTIME, &timeout,
                        NULL) == -1) {
                    kill(getpid(), SIGPIPE);  // Shit happened.
                }
            }
            // Update sequence number.
            if (outSeqNum == maxSeqNum) {
                // Correctly "overflow" sequence numbers.
                outSeqNum = 0;
            } else outSeqNum++;
        }
    }
    pthread_cleanup_pop(0);
}
