/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 19/8/2019
 * -----------------------------------------------------------------------------
 * Internal compiler definitions and macros for the RTL Library.
 */

#ifndef _LIBRTL_DEFINITIONS_H
#define _LIBRTL_DEFINITIONS_H

#include <stdint.h>
#include <pthread.h>
#include <sys/socket.h>
#include <semaphore.h>

#include "../Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h"
#include "../Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h"

/* RTL header and metadata sizes. */
#define RTL_HEAD_LEN 8
#define RTL_SYN_DATA 13

/* UDP buffers and pipes sizes. */
#define __RTL_UDP_BUF 106496
#define __RTL_PIPESZ 1048576

/* Binary flags for packets. */
#define SYN (char)0x80
#define ACK (char)0x40
#define FIN (char)0x20

/* SYN_ACK timeout (seconds). */
#define __SYNACK_TIMEO 30

/* Maximum retransmission attempts (for Output Entities). */
#define __MAX_RETR_ATTEMPTS 150
#define __RETR_ENABLE 0

/* Timings and retransmission attempts for FIN packets. */
#define __FIN_RETRS 10
#define __FIN_TIMEO_SECS 1
#define __FIN_TIMEO_NANOSECS 0

/* Constants for TCP-like packet timeout computation. */
#define __INITIAL_RTT 1000       // (ms)
#define __INITIAL_DEVRTT 0
#define __MAX_PCKT_TIMEO 3000    // (ms)
#define __MAX_PCKT_RETRS 10

/* Binary offsets. */
#define CHN_OFFST 1
#define SEQNUM_OFFST 2
#define DATALEN_OFFST 6
#define DATA_OFFST 8

/* Maximum length of a packet to be encapsulated inside a UDP datagram. */
#define UDP_MAXPLD 512

/* Maximum length of payload. */
#define RTL_MAXPLD (UDP_MAXPLD - RTL_HEAD_LEN)

/* Maximum size of a window (given sequence number space). */
#define __INIT_HEAP_ORD 31

/* Error codes for APIs. */
#define RTL_ERROR -1

/* Selective Repeat data structures. */
typedef struct {                       // RECEIVER WINDOW ARRAY ENTRY //
    unsigned char __receivedPkt;       // Internal flag.
    char __pckt[UDP_MAXPLD];           // Packet data.
} __receiver_wnd_entry;
typedef struct {                       // SENDER WINDOW ARRAY ENTRY //
    unsigned char __ackd;              // Internal flag.
    FibTreeNode *__timeNode;           // Priority queue pointer.
    char __pckt[UDP_MAXPLD];           // Packet data.
    uint64_t __transmTime;             // Transmission time (ms).
    uint64_t __tOutInterval;           // Timeout interval (ms).
    unsigned char __retrCount;         // Packet retransmissions counter.
} __sender_wnd_entry;

/* Structures for APIs return values. */
typedef struct {                       // CONNECT RESULTS //
    int outputPipeFD_write;            // Output pipe (write-end).
    int chan0PipeFD_read;              // Channel 0 (read-end).
    pthread_mutex_t *outputPipeLock;   // Output pipe mutex.
    pthread_mutex_t *_chanAVLLock;     // Channels index mutex.
    sem_t *_outputPipeSem;             // Output pipe release semaphore.
    pthread_t _inputEntityTID;         // Input Ent. thread ID.
    pthread_t _outputEntityTID;        // Output Ent. thread ID.
    AVLIntTree *_chanAVL;              // Channels index.
    pthread_mutex_t *_closeLock;       // Closing procedure mutex.
    sem_t *_ioCloseSem;                // Closing procedure semaphore.
} RTLConnectValues;
typedef struct {                       // ACCEPT RESULTS //
    unsigned int _N;                   // Window width.
    int _T;                            // Packet transmission timeout.
    unsigned char _p;                  // Simulated loss probability.
    uint32_t _initialInSeqNum;         // Initial sequence number.
    uint32_t _maxSeqNum;               // Maximum configured sequence number.
    int newSock;                       // New socket opened during handshake.
    uint32_t transID;                  // Transaction ID (in net. byte order).
} RTLAcceptValues;

/* Arguments buffers for RTL threads. */
typedef struct {                       // INPUT ENTITY ARGUMENTS //
    int sockFD;                        // UDP socket file descriptor.
    int chanDmxPipe_write;             // Channel Demux pipe (write-end).
    int inputPipeFD_write;             // Internal ACK pipe (write-end).
    uint32_t windowSize;               // Receiver window size.
    uint32_t nextInNum;                // Initial expected sequence number.
    uint32_t maxSeqNum;                // Maximum configured sequence number.
    __receiver_wnd_entry *_recvWnd;    // Receiver window structure.
    pthread_t chanDmxTID;              // Channel Demux thread ID.
    pthread_mutex_t *outputPipeLock;   // Output pipe mutex.
    pthread_mutex_t *_closeLock;       // Closing procedure mutex.
    sem_t *_outputPipeSem;             // Output pipe release semaphore.
    int outputPipeFD_write;            // Output pipe (write-end).
} _inputEntArgs;
typedef struct {                       // OUTPUT ENTITY ARGUMENTS //
    int sockFD;                        // UDP socket file descriptor.
    int tOut;                          // Packet transmission timeout.
    unsigned char lossProb;            // Simulated loss probability.
    uint32_t windowSize;               // Sender window size.
    __sender_wnd_entry *_sendWnd;      // Sender window structure.
    FibHeap *_timeHeap;                // Timers priority queue.
    int inputPipeFD_read;              // Internal ACK pipe (read-end).
    uint32_t firstOutNum;              // Initial outward sequence number.
    uint32_t maxSeqNum;                // Maximum configured sequence number.
    int outputPipeFD_read;             // Output pipe (read-end).
    int timerFD;                       // System timer file descriptor.
    sem_t *outputPipeSem;              // Output pipe release semaphore.
    sem_t *_ioCloseSem;                // Closing procedure semaphore.
    pthread_t _inputEntTID;            // Input Entity thread ID.
} _outputEntArgs;
typedef struct {                       // CHANNEL DEMUX ARGUMENTS //
    AVLIntTree *_chanAVL;              // Channels index.
    pthread_mutex_t *_chanAVLLock;     // Channels index mutex.
    int chanDmxPipe_read;              // Channel Demux pipe (read-end).
} _chanDmxArgs;

#endif
