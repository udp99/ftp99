/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 28/8/2019
 * -----------------------------------------------------------------------------
 * Main source file for the RTL Library.
 * Contains code of the communication APIs.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <errno.h>

#include "rtl.h"

#include "../Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h"

/* Opens a new channel for transmissions.
 * Takes a new channel number.
 * Creates a new internal pipe and gives write access to it to the Channel
 * Demux.
 * Returns the file descriptor of the read end, to use in upper level, or an
 * error code.
 * WARNING: This function acquires locks to work. Calling threads should not
 * be canceled/signaled or otherwise terminate while executing this one.
 */
int rtlOpenChannel(RTLConnectValues *connData, int newChan) {
    // If a suggestion was given, check and use that.
    if ((newChan <= 0) || (newChan > UINT8_MAX) || (connData == NULL)) {
        errno = EINVAL;
        return RTL_ERROR;
    }
    int chanNum = newChan;
    int newPipe[2];
    // Open new channel pipe.
    if ((pipe(newPipe) == -1) ||
        (fcntl(newPipe[0], F_SETPIPE_SZ, __RTL_PIPESZ) < __RTL_PIPESZ))
        return RTL_ERROR;
    // Add new entry to the Channel Demux's AVL Tree.
    pthread_mutex_lock(connData->_chanAVLLock);
    // Check that the tree isn't full (in our way: 256 nodes),
    // or that such channel number doesn't already exist.
    if (intSearchData(connData->_chanAVL, chanNum) != -1) {
        close(newPipe[0]);
        close(newPipe[1]);
        pthread_mutex_unlock(connData->_chanAVLLock);
        errno = EINVAL;
        return RTL_ERROR;
    }
    if (connData->_chanAVL->nodesCount >= (UINT8_MAX + 1)) {
        close(newPipe[0]);
        close(newPipe[1]);
        pthread_mutex_unlock(connData->_chanAVLLock);
        errno = ENOMEM;
        return RTL_ERROR;
    }
    // Add new entry to the tree.
    intInsert(connData->_chanAVL, chanNum, newPipe[1]);
    pthread_mutex_unlock(connData->_chanAVLLock);
    // Return new pipe's read end.
    return newPipe[0];
}

/* Closes an existing channel.
 * Accesses shared index and deletes the corresponding entry.
 * WARNING: This function acquires locks to work. Calling threads should not
 * be canceled/signaled or otherwise terminate while executing this one.
 */
int rtlCloseChannel(int channel, int chanReadFD, RTLConnectValues *connData) {
    // Sanity check.
    if ((channel <= 0) || (channel > UINT8_MAX) ||
        (chanReadFD <= STDERR_FILENO) || (connData == NULL)) {
        errno = EINVAL;
        return RTL_ERROR;
    }
    // Delete the entry (if it exists), closing the pipe.
    pthread_mutex_lock(connData->_chanAVLLock);
    int writeEnd = intSearchData(connData->_chanAVL, channel);
    if (writeEnd != -1) {
        close(writeEnd);
        close(chanReadFD);
        intDelete(connData->_chanAVL, channel);
    } else {
        pthread_mutex_unlock(connData->_chanAVLLock);
        errno = EINVAL;
        return RTL_ERROR;
    }
    pthread_mutex_unlock(connData->_chanAVLLock);
    return 0;
}

/* Sends the given amount of data on the specified channel.
 * Takes a message, splits it into packets, writes the header,
 * then forwards them to the Output Entity.
 * WARNING: This function acquires locks to work. Calling threads should not
 * be canceled/signaled or otherwise terminate while executing this one.
 */
ssize_t rtlSend(int chan, RTLConnectValues *connData, const char *buf,
        size_t len) {
    // Sanity check.
    if ((chan < 0) || (chan > UINT8_MAX) ||
        (connData == NULL) || (buf == NULL) || (len == 0)) {
        errno = EINVAL;
        return RTL_ERROR;
    }
    // Check if RTL level is still operating (client is still online).
    if (sem_trywait(connData->_ioCloseSem) == 0)
        kill(getpid(), SIGPIPE);
    char chanNum = (char)chan;
    char newPckt[UDP_MAXPLD];
    ssize_t sentBytes = 0;
    uint16_t newDataLen = 0;
    uint16_t newDataLen_net = 0;
    // Compute the number of packets needed to send the entire message.
    uint64_t pcktsNum = len / RTL_MAXPLD;
    if ((len % RTL_MAXPLD) != 0) pcktsNum++;
    // Packetize the buffer and send each packet out.
    for (uint64_t i = 0; i < pcktsNum; i++) {
        // Add channel number to packet.
        memset(newPckt, 0, UDP_MAXPLD);
        newPckt[CHN_OFFST] = chanNum;
        // Add data and its length.
        if ((len - sentBytes) >= RTL_MAXPLD) newDataLen = RTL_MAXPLD;
        else newDataLen = len - sentBytes;
        newDataLen_net = htons(newDataLen);
        memcpy(newPckt + DATA_OFFST, buf + sentBytes, newDataLen);
        memcpy(newPckt + DATALEN_OFFST, &newDataLen_net, 2);
        sentBytes += newDataLen;
        // Forward packet to output entity.
        pthread_mutex_lock(connData->outputPipeLock);
        if (write(connData->outputPipeFD_write, newPckt, UDP_MAXPLD) == -1) {
            pthread_mutex_unlock(connData->outputPipeLock);
            return RTL_ERROR;
        }
        sem_wait(connData->_outputPipeSem);
        pthread_mutex_unlock(connData->outputPipeLock);
    }
    return sentBytes;
}

/* Receives up to the given amount of data from the specified channel,
 * placing it in the provided buffer.
 * Reads from the corresponding pipe in the Channel Demux.
 */
ssize_t rtlRecv(int chan, int chanReadFD, RTLConnectValues *connData,
        char *buf, size_t len) {
    // Sanity check.
    // WARNING: We have no way to check the validity of the given pipe's fd,
    // other than a failure of the read, without checking the tree.
    // We don't want to acquire that lock now.
    // Also, if someone else closes this channel while there's a transfer
    // going on, you called the wrath of UNIX upon yourself.
    if ((chan < 0) || (chan > UINT8_MAX) ||
        ((connData == NULL) && (chanReadFD <= STDERR_FILENO)) ||
        (buf == NULL) || (len == 0)) {
        errno = EINVAL;
        return RTL_ERROR;
    }
    // Get the correct pipe to read from.
    int pipeToRead;
    if (chan != 0) pipeToRead = chanReadFD;
    else pipeToRead = connData->chan0PipeFD_read;
    // Read stuff.
    return read(pipeToRead, buf, len);
}
