/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 5/9/2019
 * -----------------------------------------------------------------------------
 * Common definitions for the FTP99 applications.
 */

#include <sys/types.h>
#include <pthread.h>

#include "librtl/rtl.h"
#include "Libraries/DoubleLinkedList/doubleLinkedList.h"

/* Maximum file name length (in characters). */
#define FNAME_MAX 250

/* Maximum command packet length (in bytes). */
#define HDR_LEN 3
#define CHAN_LEN 1
#define FILESZ_LEN 8
#define CMD_MAXLEN (HDR_LEN +CHAN_LEN + FILESZ_LEN + FNAME_MAX)

/* Maximum number of clients (for servers). */
#define CLIENTS_MAX 100

/* Command identifiers. */
#define GET "GET"
#define PUT "PUT"
#define LST "LST"
#define OKG "OKG"
#define OKP "OKP"
#define ERR "ERR"
#define CLS "CLS"

/* Argument buffer for server's worker threads. */
typedef struct {
    RTLConnectValues *connData;
    int newChan;
    int newChanPipe;
    int reqFile;
    size_t fileSize;
    pthread_t workerTID;
    ulong clNum;
    unsigned char isServer;
    Record *thisRec;
    char fileName[FNAME_MAX + 1];
} __workerArgs;

/* Macros to flip 64-bit integers to/from network byte order. */
#if __BIG_ENDIAN__
#define htonll(x) (x)
#define ntohll(x) (x)
#else
#define htonll(x) ((uint64_t)htonl((x) & 0xFFFFFFFF) << 32) | htonl((x) >> 32)
#define ntohll(x) ((uint64_t)ntohl((x) & 0xFFFFFFFF) << 32) | ntohl((x) >> 32)
#endif

/* Stack size and buffers for new processes doing directory listings. */
#define __LS_STACKSIZE 4096
#define LS_BUFSIZE 1500
