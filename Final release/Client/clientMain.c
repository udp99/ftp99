/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 5/9/2019
 * -----------------------------------------------------------------------------
 * Main source file for the FTP99 Client application.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <arpa/inet.h>
#include <errno.h>

#include "../FTP99.h"
#include "../librtl/rtl.h"

#include "clientMain.h"

RTLConnectValues *connData = NULL;
DLList *workers = NULL;
pthread_mutex_t *workersLock = NULL;

int opCode;
char insertedCode[2];

extern pthread_mutex_t consoleLock;

/* The works. */
int main(int argc, char **argv) {
    char *addr, *port, *windowSize, *timeout, *loss;
    char dynamicInput = 0;

    // Get connection parameters.
    if (argc == 1) {
        if (getUserInput(&addr, &port, &windowSize, &timeout, &loss))
            exit(EXIT_FAILURE);
        dynamicInput = 1;
    } else {
        if (parseCmdLine(argc, argv, &addr, &port, &windowSize, &timeout,
                         &loss))
            exit(EXIT_FAILURE);
    }

    // Convert parameters.
    ushort portNumber = strtol(port, NULL, 10);
    unsigned int N = strtol(windowSize, NULL, 10);
    int T = (int)strtol(timeout, NULL, 10);
    unsigned char p = strtol(loss, NULL, 10);

    // Configure signals behaviour.
    struct sigaction act, act2, consoleAct;
    memset(&act, 0, sizeof(struct sigaction));
    memset(&act2, 0, sizeof(struct sigaction));
    memset(&consoleAct, 0, sizeof(struct sigaction));
    sigset_t mask, mask2, consMask;
    sigfillset(&mask); sigdelset(&mask, SIGALRM); sigdelset(&mask, SIGPIPE);
    sigfillset(&mask2);
    sigfillset(&consMask); sigdelset(&consMask, SIGTERM);
    sigdelset(&consMask, SIGPIPE);
    act.sa_handler = clientCloseHandler;
    act2.sa_handler = clientCloseHandler;
    consoleAct.sa_handler = consoleHandler;
    act.sa_mask = mask;
    act2.sa_mask = mask2;
    consoleAct.sa_mask = consMask;
    act.sa_flags = 0;
    act2.sa_flags = 0;
    consoleAct.sa_flags = 0;
    if ((sigaction(SIGINT, &consoleAct, NULL) != 0) ||
        (sigaction(SIGTERM, &act, NULL) != 0) ||
        (sigaction(SIGALRM, &act2, NULL) != 0) ||
        (sigaction(SIGPIPE, &act2, NULL) != 0)) {
        fprintf(stderr, "ERROR: Failed to configure signal handlers.\n");
        perror("sigaction");
        exit(EXIT_FAILURE);
    }
    sigemptyset(&mask);
    sigaddset(&mask, SIGINT); sigaddset(&mask, SIGTERM);

    // Initialize console mutex.
    if (pthread_mutex_init(&consoleLock, NULL) != 0) {
        fprintf(stderr, "ERROR: Failed to initialize console mutex.\n");
        exit(EXIT_FAILURE);
    }

    // Initialize worker threads list.
    workers = createDLList();
    if (workers == NULL) {
        fprintf(stderr, "ERROR: Failed to create workers list.\n");
        exit(EXIT_FAILURE);
    }

    // Initialize workers list mutex.
    workersLock = (pthread_mutex_t *)calloc(1, sizeof(pthread_mutex_t));
    if ((workersLock == NULL) || (pthread_mutex_init(workersLock, NULL) != 0)) {
        fprintf(stderr, "ERROR: Failed to initialize workers list mutex.\n");
        exit(EXIT_FAILURE);
    }

    // Initialize UDP socket.
    int sockFD = rtlInit(NULL, 0);
    if (sockFD == RTL_ERROR) {
        fprintf(stderr, "ERROR: Failed to initialize UDP socket.\n");
        perror("rtlInit");
        if (dynamicInput) {
            free(port);
            free(addr);
            free(timeout);
            free(windowSize);
            free(loss);
        }
        exit(EXIT_FAILURE);
    } else if (errno == EINVAL) {
        fprintf(stderr, "ERROR: Invalid address.\n");
        if (dynamicInput) {
            free(port);
            free(addr);
            free(timeout);
            free(windowSize);
            free(loss);
        }
        exit(EXIT_FAILURE);
    }

    // Connect to server.
    if ((connData = rtlConnect(addr, portNumber, sockFD, N, T, p)) == NULL) {
        fprintf(stderr, "ERROR: Failed to connect to server.\n");
        perror("rtlConnect");
        if (dynamicInput) {
            free(port);
            free(addr);
            free(timeout);
            free(windowSize);
            free(loss);
        }
        exit(EXIT_FAILURE);
    }

    // Free now unnecessary variables.
    if (dynamicInput) {
        free(port);
        free(addr);
        free(timeout);
        free(windowSize);
        free(loss);
    }

    printf("Connection to server established!\n"
           "Press Ctrl+C to enter a command.\n");

    // Client operations loop.

    for (;;) {
        // Wait for user input.
        pause();

        // Block SIGINT/SIGTERM during operations.
        pthread_sigmask(SIG_BLOCK, &mask, NULL);

        switch (opCode) {
            case 1:
                list();
                break;
            case 2:
                get();
                break;
            case 3:
                put();
                break;
            case 4:
                raise(SIGTERM);
                break;
            default:
                printf("ERROR: Invalid operation code.\n");
                break;
        }

        // Unblock SIGINT/SIGTERM.
        pthread_sigmask(SIG_UNBLOCK, &mask, NULL);
    }

    return 0;
}

/* Parses configuration parameters. */
int parseCmdLine(int argc, char **argv, char **addr, char **port,
                 char **windowSize, char **timeout, char **loss) {
    int n = 1;

    *port = NULL;
    *addr = NULL;
    *windowSize = NULL;
    *loss = NULL;
    *timeout = NULL;

    while (n < argc) {
        if (!strncmp(argv[n], "-a", 2) || !strncmp(argv[n], "-A", 2))
            *addr = argv[++n];
        else if (!strncmp(argv[n], "-p", 2) || !strncmp(argv[n], "-P", 2))
            *port = argv[++n];
        else if (!strncmp(argv[n], "-n", 2) || !strncmp(argv[n], "-N", 2))
            *windowSize = argv[++n];
        else if (!strncmp(argv[n], "-t", 2) || !strncmp(argv[n], "-T", 2))
            *timeout = argv[++n];
        else if (!strncmp(argv[n], "-l", 2) || !strncmp(argv[n], "-L", 2))
            *loss = argv[++n];
        n++;
    }
    if ((argc < 5) || ((*port) == NULL) || ((*addr) == NULL)) {
        fprintf(stderr, "Usage: ftp99_client -a address -p port -n windowSize "
                       "-t timeout -l lossProbability\n");
        return -1;
    }

    return 0;
}

/* Reads configuration parameters. */
int getUserInput(char **addr, char **port, char **windowSize,
                 char **timeout, char **loss) {
    // Initialize variables.
    *port = (char *)calloc(6, sizeof(char));
    *addr = (char *)calloc(INET_ADDRSTRLEN, sizeof(char));
    *timeout = (char *)calloc(8, sizeof(char));
    *windowSize = (char *)calloc(11, sizeof(char));
    *loss = (char *)calloc(3, sizeof(char));

    if ((*port == NULL) || (*addr == NULL) || (*timeout == NULL) ||
        (*windowSize == NULL) || (*loss == NULL)) {
        fprintf(stderr, "ERROR: Cannot initialize variables.\n");
        free(*port);
        free(*addr);
        free(*timeout);
        free(*windowSize);
        free(*loss);
        return -1;
    }

    // Get values from user.
    printf("Enter address: ");
    getInput(INET_ADDRSTRLEN, *addr);

    printf("Enter port: ");
    getInput(6, *port);

    printf("Enter window size: ");
    getInput(11, *windowSize);

    printf("Enter timeout (0 for TCP-like): ");
    getInput(8, *timeout);

    printf("Enter loss probability [0, 99]: ");
    getInput(3, *loss);

    return 0;
}

/* Reads user input from command line. */
char *getInput(unsigned int len, char *string) {
    // Get len - 1 character from stdin.
    char c, toFlush = 0;
    unsigned int i = 0;
    for (; i < len; i++) {
        fread(&c, sizeof(char), 1, stdin);
        if (c == '\n') {
            string[i] = '\0';
            break;
        } else string[i] = c;
    }

    // Add terminator character if the user inserts more than len characters.
    if ((i == (len - 1)) && (string[i] != '\0')) {
        string[i] = '\0';
        toFlush = 1;
    }

    // Flush stdin buffer if necessary.
    if (toFlush == 1) flush(stdin);

    return string;
}

/* Requests and prints a remote directory listing. */
int list(void) {
    char *recvBuff;
    size_t recvBytes, recvRes, buffSize;

    // Initialize buffer.
    buffSize = 1000;
    recvBuff = calloc(buffSize, sizeof(char));
    if (recvBuff == NULL) {
        fprintf(stderr, "ERROR: Failed to initialize buffer.\n");
        return -1;
    }

    // Send LST request on channel 0.
    if (rtlSend(0, connData, LST, 3) != 3) {
        fprintf(stderr, "ERROR: Failed to send request.\n");
        perror("rtlSend");
        raise(SIGPIPE);
    }

    // Wait for answer. Check if there is an error too.
    recvBytes = 0;
    do {
        recvRes = rtlRecv(0, 0, connData,  recvBuff + recvBytes,
                          3 - recvBytes);
        if (recvRes == RTL_ERROR) {
            fprintf(stderr, "ERROR: Failed to receive message.\n");
            perror("rtlRecv");
            raise(SIGPIPE);
        }
        recvBytes += recvRes;
    } while (recvBytes != 3);
    if (strncmp(recvBuff, ERR, 3) == 0) {
        fprintf(stderr, "ERROR: ");
        char errMessage[81];
        for (;;) {
            memset(errMessage, 0, 81);
            recvRes = rtlRecv(0, 0, connData, errMessage, 80);
            if (recvRes == RTL_ERROR) {
                fprintf(stderr, "\nERROR: Failed to receive error message.\n");
                perror("rtlRecv");
                free(recvBuff);
                raise(SIGPIPE);
            }
            fprintf(stderr, "%s", errMessage);
            if ((recvRes == 80) || (errMessage[recvRes - 1] == '\0')) break;
        }
        fprintf(stderr, "\n");
        free(recvBuff);
        return -1;
    }

    // Receive ls output.
    do {
        if (buffSize - recvBytes < 100) {
            buffSize += 1000;
            recvBuff = reallocarray(recvBuff, buffSize, sizeof(char));
            if (recvBuff == NULL) {
                fprintf(stderr, "ERROR: Failed to realloc buffer.\n");
                raise(SIGPIPE);
            }
            memset(recvBuff + recvBytes, 0, buffSize - recvBytes);
        }
        recvRes = rtlRecv(0, 0, connData,  recvBuff + recvBytes,
                          buffSize - recvBytes - 1);
        if (recvRes == RTL_ERROR) {
            fprintf(stderr, "ERROR: Failed to receive message.\n");
            perror("rtlRecv");
            free(recvBuff);
            raise(SIGPIPE);
        }
        recvBytes += recvRes;
    } while (strstr(recvBuff, "\r\n") == NULL);

    // Print remote directory listing.
    printf("%s", recvBuff);
    free(recvBuff);

    return 0;

}

/* Requests and downloads a file from the server. */
int get(void) {
    char filename[FNAME_MAX + 1];
    int channelFD, fileFD;
    unsigned char channel;
    size_t getReqLen, recvRes, recvBytes;
    uint64_t fileSizeNet;
    __workerArgs *opArgs;
    Record *workerThreadRecord;

    // Get file name from user.
    for (;;) {
        memset(filename, 0, FNAME_MAX + 1);
        printf("Enter file name: ");
        getInput(FNAME_MAX, filename);

        // Check if name is valid.
        if ((strstr(filename, "..") != NULL) ||
            (filename[strlen(filename) - 1] == '/'))
            printf("ERROR: Invalid name.\n");
        else break;
    }

    // Initialize thread args.
    opArgs = (__workerArgs *)calloc(1, sizeof(__workerArgs));
    if (opArgs == NULL) {
        fprintf(stderr, "ERROR: Failed to initialize thread arguments.\n");
        raise(SIGPIPE);
        return -1;
    }

    // Create file.
    fileFD = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0666);
    if (fileFD == -1) {
        fprintf(stderr, "ERROR: Failed to create file.\n");
        perror("open");
        free(opArgs);
        raise(SIGPIPE);
    }

    // Create worker thread record.
    workerThreadRecord = createRecord(opArgs);
    if (workerThreadRecord == NULL) {
        fprintf(stderr, "ERROR: Failed to create worker thread record.\n");
        close(fileFD);
        free(opArgs);
        raise(SIGPIPE);
    }

    // Open reception channel.
    channel = fileFD;
    if ((channelFD = rtlOpenChannel(connData, channel)) == -1) {
        fprintf(stderr, "ERROR: Failed to open new channel.\n");
        perror("rtlOpenChannel");
        close(fileFD);
        free(opArgs);
        eraseRecord(workerThreadRecord);
        raise(SIGPIPE);
    }

    // Prepare GET request.
    getReqLen = 3 + sizeof(unsigned char) + strlen(filename) + 1;
    char getRequest[getReqLen];
    memset(getRequest, 0, getReqLen);
    // Add message type.
    memcpy(getRequest, GET, 3);
    // Add channel ID.
    memcpy(getRequest + 3, &channel, sizeof(unsigned char));
    // Add file name.
    memcpy(getRequest + 3 + sizeof(unsigned char), filename, strlen(filename));

    // Send GET request.
    if (rtlSend(0, connData, getRequest, getReqLen) != getReqLen) {
        fprintf(stderr, "ERROR: Failed to send GET request.\n");
        perror("rtlSend");
        rtlCloseChannel(channel, channelFD, connData);
        free(opArgs);
        eraseRecord(workerThreadRecord);
        close(fileFD);
        raise(SIGPIPE);
    }

    // Wait for answer. Check for errors too.
    char answer[3];
    memset(answer, 0, 3);
    recvBytes = 0;
    do {
        recvRes = rtlRecv(0, 0, connData,  answer + recvBytes,
                          3 - recvBytes);
        if (recvRes == RTL_ERROR) {
            fprintf(stderr, "ERROR: Failed to receive message.\n");
            perror("rtlRecv");
            rtlCloseChannel(channel, channelFD, connData);
            free(opArgs);
            eraseRecord(workerThreadRecord);
            close(fileFD);
            raise(SIGPIPE);
        }
        recvBytes += recvRes;
    } while (recvBytes != 3);
    if (strncmp(answer, ERR, 3) == 0) {
        fprintf(stderr, "ERROR: ");
        char errMessage[81];
        for (;;) {
            memset(errMessage, 0, 81);
            recvRes = rtlRecv(0, 0, connData, errMessage, 80);
            if (recvRes == RTL_ERROR) {
                fprintf(stderr, "\nERROR: Failed to receive error message.\n");
                perror("rtlRecv");
                free(opArgs);
                eraseRecord(workerThreadRecord);
                close(fileFD);
                raise(SIGPIPE);
            }
            fprintf(stderr, "%s", errMessage);
            if ((recvRes == 80) || (errMessage[recvRes - 1] == '\0')) break;
        }
        fputc('\n', stderr);
        rtlCloseChannel(channel, channelFD, connData);
        free(opArgs);
        eraseRecord(workerThreadRecord);
        close(fileFD);
        return -1;
    } else if (strncmp(answer, OKG, 3) == 0) {
        // Load file size.
        recvBytes = 0;
        do {
            recvRes = rtlRecv(0, 0, connData,
                         (char *)(&fileSizeNet) + recvBytes,
                         sizeof(uint64_t) - recvBytes);
            if (recvRes == RTL_ERROR) {
                fprintf(stderr, "ERROR: Failed to receive message.\n");
                perror("rtlRecv");
                rtlCloseChannel(channel, channelFD, connData);
                free(opArgs);
                eraseRecord(workerThreadRecord);
                close(fileFD);
                raise(SIGPIPE);
            }
            recvBytes += recvRes;
        } while (recvBytes != sizeof(uint64_t));


        // Set worker thread arguments.
        sigset_t oldSet, ignSet;
        sigfillset(&ignSet);
        pthread_sigmask(SIG_SETMASK, &ignSet, &oldSet);

        opArgs->isServer = 0;
        opArgs->newChan = channel;
        opArgs->clNum = 0;
        opArgs->connData = connData;
        opArgs->newChanPipe = channelFD;
        opArgs->fileSize = ntohll(fileSizeNet);
        opArgs->reqFile = fileFD;
        opArgs->thisRec = workerThreadRecord;
        memcpy(opArgs->fileName, filename, strlen(filename));

        // Spawn worker thread to process this request.
        if (pthread_create(&(opArgs->workerTID), NULL,
                           getter, opArgs) != 0) {
            fprintf(stderr, "ERROR: Failed to spawn worker thread.\n");
            free(opArgs);
            eraseRecord(workerThreadRecord);
            rtlCloseChannel(channel, channelFD, connData);
            pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
            close(fileFD);
            raise(SIGPIPE);
        }
        pthread_mutex_lock(workersLock);
        Record *newWorker = addAsLastRecord(workerThreadRecord, workers);
        pthread_mutex_unlock(workersLock);
        opArgs->thisRec = newWorker;
        pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
    } else {
        fprintf(stderr, "ERROR: Unknown message from server.\n");
        rtlCloseChannel(channel, channelFD, connData);
        free(opArgs);
        eraseRecord(workerThreadRecord);
        close(fileFD);
        return -1;
    }

    return 0;
}

/* Uploads a file onto the server. */
int put(void) {
    char filename[FNAME_MAX + 1];
    int fileFD, channelFD;
    unsigned char channel;
    size_t putReqLen, recvRes, recvBytes;
    __workerArgs *opArgs;
    Record *workerThreadRecord;

    for (;;) {
        memset(filename, 0, FNAME_MAX + 1);

        // Get file name from user.
        printf("Enter file name: ");
        getInput(FNAME_MAX, filename);

        // Check if name is valid.
        if ((strstr(filename, "..") != NULL) ||
            (filename[strlen(filename) - 1] == '/'))
            printf("ERROR: Invalid name.\n");
        else break;
    }

    // Check if the file exists and get its size.
    size_t fileLen = 0;
    struct stat fileStat;
    memset(&fileStat, 0, sizeof(struct stat));
    if (((fileFD = open(filename, O_RDONLY)) == -1) ||
        (stat(filename, &fileStat) != 0)) {
        if (errno == EINVAL) {
            printf("ERROR: Selected file does not exist or you do not have "
                   "read privileges.\n");
            return -1;
        } else {
            fprintf(stderr, "ERROR: Failed to open file.\n");
            perror("open/stat");
            return -1;
        }
    }
    fileLen = fileStat.st_size;
    uint64_t fileLen_net = htonll(fileLen);

    // Initialize thread args.
    opArgs = (__workerArgs *)calloc(1, sizeof(__workerArgs));
    if (opArgs == NULL) {
        fprintf(stderr, "ERROR: Failed to initialize thread arguments");
        close(fileFD);
        raise(SIGPIPE);
        return -1;
    }

    // Create worker thread record
    workerThreadRecord = createRecord(opArgs);
    if (workerThreadRecord == NULL) {
        fprintf(stderr, "ERROR: Failed to create worker thread record\n");
        close(fileFD);
        free(opArgs);
        raise(SIGPIPE);
    }

    // Open PUT channel.
    channel = fileFD;
    if ((channelFD = rtlOpenChannel(connData, channel)) == -1) {
        fprintf(stderr, "ERROR: Failed to open new channel.\n");
        perror("rtlOpenChannel");
        close(fileFD);
        free(opArgs);
        eraseRecord(workerThreadRecord);
        raise(SIGPIPE);
    }

    // Prepare PUT request.
    putReqLen = 3 + sizeof(unsigned char) + sizeof(uint64_t)
                + strlen(filename) + 1;
    char putRequest[putReqLen];
    memset(putRequest, 0, putReqLen);
    // Add message type.
    memcpy(putRequest, PUT, 3);
    // Add channel number.
    memcpy(putRequest + 3, &channel, sizeof(unsigned char));
    // Add file size.
    memcpy(putRequest + 3 + sizeof(unsigned char), &fileLen_net,
            sizeof(uint64_t));
    // Add file name.
    memcpy(putRequest + 3 + sizeof(unsigned char) + sizeof(uint64_t), filename,
            strlen(filename));

    // Send PUT request.
    if (rtlSend(0, connData, putRequest, putReqLen) != putReqLen) {
        fprintf(stderr, "ERROR: Failed to send request.\n");
        perror("rtlSend");
        rtlCloseChannel(channel, channelFD, connData);
        eraseRecord(workerThreadRecord);
        close(fileFD);
        free(opArgs);
        raise(SIGPIPE);
    }

    // Wait for answer. Check for errors too.
    char answer[3];
    memset(answer, 0, 3);
    recvBytes = 0;
    do {
        recvRes = rtlRecv(0, 0, connData,  answer + recvBytes,
                          3 - recvBytes);
        if (recvRes == RTL_ERROR) {
            fprintf(stderr, "ERROR: Failed to receive message.\n");
            perror("rtlRecv");
            rtlCloseChannel(channel, channelFD, connData);
            eraseRecord(workerThreadRecord);
            close(fileFD);
            free(opArgs);
            raise(SIGPIPE);
        }
        recvBytes += recvRes;
    } while (recvBytes != 3);
    if (strncmp(answer, ERR, 3) == 0) {
        fprintf(stderr, "ERROR: ");
        char errMessage[81];
        for (;;) {
            memset(errMessage, 0, 81);
            recvRes = rtlRecv(0, 0, connData, errMessage, 80);
            if (recvRes == RTL_ERROR) {
                fprintf(stderr, "\nERROR: Failed to receive error message.\n");
                perror("rtlRecv");
                rtlCloseChannel(channel, channelFD, connData);
                free(opArgs);
                eraseRecord(workerThreadRecord);
                close(fileFD);
                raise(SIGPIPE);
            }
            fprintf(stderr, "%s", errMessage);
            if ((recvRes == 80) || (errMessage[recvRes - 1] == '\0')) break;
        }
        fputc('\n', stderr);
        rtlCloseChannel(channel, channelFD, connData);
        free(opArgs);
        eraseRecord(workerThreadRecord);
        close(fileFD);
        return -1;
    } else if (strncmp(answer, OKP, 3) == 0) {
        sigset_t oldSet, ignSet;
        sigfillset(&ignSet);
        pthread_sigmask(SIG_SETMASK, &ignSet, &oldSet);

        // Set worker thread arguments.
        opArgs->isServer = 0;
        opArgs->clNum = 0;
        opArgs->connData = connData;
        opArgs->fileSize = fileLen;
        opArgs->newChan = channel;
        opArgs->newChanPipe = channelFD;
        opArgs->reqFile = fileFD;
        memcpy(opArgs->fileName, filename, strlen(filename));

        // Spawn worker thread to process this request.
        if (pthread_create(&(opArgs->workerTID), NULL,
                           putter, opArgs) != 0) {
            fprintf(stderr, "ERROR: Failed to spawn worker thread.\n");
            free(opArgs);
            eraseRecord(workerThreadRecord);
            close(fileFD);
            pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
            raise(SIGPIPE);
        }
        pthread_mutex_lock(workersLock);
        Record *newWorker = addAsLastRecord(workerThreadRecord, workers);
        pthread_mutex_unlock(workersLock);
        opArgs->thisRec = newWorker;
        pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
    } else {
        fprintf(stderr, "ERROR: Unknown message from server.\n");
        free(opArgs);
        eraseRecord(workerThreadRecord);
        close(fileFD);
        return -1;
    }

    return 0;
}

/* Signal handler to shut the application down. */
void clientCloseHandler(int signo) {
    size_t recvBytes, recvRes;

    // Close active worker threads.
    // Momentarily block all signals to avoid deadlocks.
    sigset_t ignSet; sigfillset(&ignSet);
    sigset_t oldSet; sigemptyset(&oldSet);
    pthread_sigmask(SIG_SETMASK, &ignSet, &oldSet);
    if (workersLock != NULL) {
        pthread_mutex_lock(workersLock);
        if (workers->recsCount > 0) {
            Record *worker = workers->first;
            while (worker != NULL) {
                pthread_t workerTID = ((__workerArgs *) (worker->recData))
                        ->workerTID;
                pthread_cancel(workerTID);
                worker = worker->next;
                popFirst(workers);
            }
        }
        pthread_mutex_destroy(workersLock);
        eraseList(workers);
        workersLock = NULL;
    }
    pthread_sigmask(SIG_SETMASK, &oldSet, NULL);

    pthread_mutex_destroy(&consoleLock);

    // Shutdown according to error severity.
    switch (signo) {
        case SIGALRM:
        case SIGPIPE:
            fprintf(stderr, "\nERROR: Connection error.\n");
            fprintf(stderr, "\nShutting down...\n");
            rtlClose(connData);
            exit(EXIT_FAILURE);
            break;
        case SIGTERM:
        case SIGINT:
            printf("\b\b  \nClient is now closing.\n");
            printf("Sending disconnection message...\n");

            // Send CLS message.
            if (rtlSend(0, connData, CLS, 3) != 3) {
                fprintf(stderr, "\nERROR: Failed to send message.\n");
                perror("rtlSend");
                rtlClose(connData);
                exit(EXIT_FAILURE);
            }

            // Wait for answer. Check for errors too.
            // This action times out to account for disconnections.
            char answer[3];
            memset(answer, 0, 3);
            recvBytes = 0;

            alarm(__CLS_TIMEO);
            do {
                recvRes = rtlRecv(0, 0, connData,  answer + recvBytes,
                                  3 - recvBytes);
                if (recvRes == RTL_ERROR) {
                    fprintf(stderr, "\nERROR: Failed to receive message.\n");
                    perror("rtlRecv");
                    rtlClose(connData);
                    exit(EXIT_FAILURE);
                }
                recvBytes += recvRes;
            } while (recvBytes < 3);
            alarm(0);

            if (strncmp(answer, CLS, 3) == 0) {
                rtlClose(connData);
                printf("Connection closed. Goodbye!\n");
                exit(EXIT_SUCCESS);
            } else if (strncmp(answer, ERR, 3) == 0) {
                fprintf(stderr, "ERROR: ");
                char errMessage[81];
                for (;;) {
                    memset(errMessage, 0, 81);
                    recvRes = rtlRecv(0, 0, connData, errMessage, 80);
                    if (recvRes == RTL_ERROR) {
                        fprintf(stderr, "\nERROR: Failed to receive error "
                                        "message .\n");
                        perror("rtlRecv");
                        rtlClose(connData);
                        exit(EXIT_FAILURE);
                    }
                    fprintf(stderr, "%s", errMessage);
                    if ((recvRes == 80) || (errMessage[recvRes - 1] == '\0'))
                        break;
                }
                fputc('\n', stderr);
                rtlClose(connData);
                exit(EXIT_FAILURE);
            } else {
                fprintf(stderr, "ERROR: Unknown message.\n");
                rtlClose(connData);
                exit(EXIT_FAILURE);
            }
            break;
        default:
            fprintf(stderr, "\nERROR: Unknown error.\n");
            exit(EXIT_FAILURE);
            break;
    }
}

/* Processes a command from the user. */
void consoleHandler(int sig) {
    (void)sig;
    memset(insertedCode, 0, 2);
    pthread_mutex_lock(&consoleLock);
    printf("\b\bSelect an operation:\n"
           "1) List files on server.\n"
           "2) Get file from server.\n"
           "3) Put file onto server.\n"
           "4) Close this client.\n"
           "Operation code: ");
    getInput(2, insertedCode);
    pthread_mutex_unlock(&consoleLock);
    opCode = (int)strtol(insertedCode, NULL, 10);
}
