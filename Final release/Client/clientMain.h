/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 10/9/2019
 * -----------------------------------------------------------------------------
 * Header file for the FTP99 Client application.
 */

#ifndef FTP99_CLIENTMAIN_H
#define FTP99_CLIENTMAIN_H

#define __CLS_TIMEO 60  // secs
#define flush(stdin) while (getchar() != '\n')

int getUserInput(char **addr, char **port, char **windowSize,
                 char **timeout, char **loss);
int parseCmdLine(int argc, char **argv, char **addr, char **port,
                 char **windowSize, char **timeout, char **loss);
char *getInput(unsigned int len, char *string);

int list(void);
int get(void);
int put(void);

void *getter(void *args);
void *putter(void *args);

void clientCloseHandler(int signo);
void consoleHandler(int sig);

#endif
