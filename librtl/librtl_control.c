/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 19/8/2019
 * -----------------------------------------------------------------------------
 * Main source file for the RTL Library.
 * Contains code of the control APIs.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <semaphore.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include "rtl.h"

#include "../Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h"
#include "../Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h"

/* Internal threads and routines. */
void *_inputEntity(void *args);
void *_outputEntity(void *args);
void *_chanDmx(void *args);

/* Routine to check window size and set maximum sequence number. */
uint32_t _checkWnd(uint32_t N) {
    if (N == 0) return 0;  // Sanity check.
    uint32_t maxValue;
    if ((UINT32_MAX % N) == (N - 1)) {
        maxValue = UINT32_MAX;
    } else maxValue = UINT32_MAX - (UINT32_MAX % N) - 1;
    if ((uint64_t)maxValue >= (((uint64_t)N << 2) - 1)) return maxValue;
    return 0;
}

/* Initializes the underlying UDP socket and binds it to the provided
 * interface in servers.
 * Requires optional port number and IP address in ASCII format (or an empty
 * string to bind to any interface).
 * Returns such socket's FD or an error code.
 */
int rtlInit(const char *ip_addr, ushort port) {
    // Creates UDP socket.
    int sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if ((sockfd == -1) || (fcntl(sockfd, F_SETFD, FD_CLOEXEC) != 0)) {
        if (sockfd != -1) close(sockfd);
        return RTL_ERROR;
    }
    // Enlarges its buffers (checking the result).
    int bufSize = __RTL_UDP_BUF;
    if ((setsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &bufSize, sizeof(int))
         != 0) ||
        (setsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &bufSize, sizeof(int)) != 0))
        return RTL_ERROR;
    bufSize = 0;
    socklen_t optLen = sizeof(int);
    if ((getsockopt(sockfd, SOL_SOCKET, SO_RCVBUF, &bufSize, &optLen) != 0) ||
        (bufSize < (2 * __RTL_UDP_BUF)) ||
        (getsockopt(sockfd, SOL_SOCKET, SO_SNDBUF, &bufSize, &optLen) != 0) ||
        (bufSize < (2 * __RTL_UDP_BUF)))
        return RTL_ERROR;
    // And binds it to the provided IP/Port.
    if (ip_addr != NULL) {
        struct sockaddr_in serverAddr;
        memset(&serverAddr, 0, sizeof(struct sockaddr_in));
        serverAddr.sin_family = AF_INET;
        serverAddr.sin_port = htons(port);
        if (strlen(ip_addr) == 0) serverAddr.sin_addr.s_addr = INADDR_ANY;
        else if (inet_pton(AF_INET, ip_addr, &(serverAddr.sin_addr.s_addr))
                 != 1) {
            errno = EINVAL;
            return RTL_ERROR;
        }
        if (bind(sockfd, (struct sockaddr *)&serverAddr,
                 sizeof(struct sockaddr_in)) == -1) {
            close(sockfd);
            return RTL_ERROR;
        }
    }
    return sockfd;
}

/* Establishes a connection to the server using the underlying transport
 * layer, with the UDP protocol.
 * Requires server addressing information and configuration parameters like
 * window size, packets timeout, loss probability.
 * Returns a buffer of values with: the write end of a pipe used to send
 * data, the address of a lock used to access such pipe,
 * and the read end of a pipe used to receive data from channel 0.
 * Such structure can be passed to other APIs to reference the correct
 * connection in multithreaded applications.
 * Returns NULL on error.
 */
RTLConnectValues *rtlConnect(const char *srv_addr, ushort port, int sockFD,
        unsigned int N, int T, unsigned char p) {
    // Check input arguments.
    if ((srv_addr == NULL) || (strlen(srv_addr) == 0) ||
        (port == 0) ||
        (T < 0) ||
        (p > 99)) {
        errno = EINVAL;
        return NULL;
    }
    if (sockFD <= 0) {
        errno = EBADF;
        return NULL;
    }
    // Configure connection parameters.
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    if (inet_pton(AF_INET, srv_addr, &(addr.sin_addr.s_addr)) != 1)
        return NULL;
    // Check window size and set maximum sequence number.
    uint32_t maxValue = _checkWnd(N);
    if (maxValue == 0) {
        errno = EINVAL;
        return NULL;
    }
    // Generate initial sequence number and set base number.
    struct drand48_data myRandData;
    memset(&myRandData, 0, sizeof(struct drand48_data));
    srand48_r(getpid(), &myRandData);
    long int randNum = 0;
    lrand48_r(&myRandData, &randNum);
    uint32_t nextOutNum = (uint32_t)randNum % maxValue;
    uint32_t initSeqNum = htonl(nextOutNum);
    if (nextOutNum == maxValue) nextOutNum = 0;
    else nextOutNum++;
    // Generate transaction ID.
    lrand48_r(&myRandData, &randNum);
    uint32_t transID = (uint32_t)randNum;
    // Build SYN packet.
    char myPckt[RTL_HEAD_LEN + RTL_SYN_DATA];
    char synAck[UDP_MAXPLD];
    memset(myPckt, 0, RTL_HEAD_LEN + RTL_SYN_DATA);
    memset(synAck, 0, UDP_MAXPLD);
    myPckt[0] = SYN;  // SYN flag.
    memcpy(myPckt + SEQNUM_OFFST, &initSeqNum, 4);  // Init. seqnum..
    uint16_t synDataLen = htons(RTL_SYN_DATA);
    memcpy(myPckt + DATALEN_OFFST, &synDataLen, 2);  // SYN data len..
    uint32_t wndSize = htonl(N);
    uint32_t tOut = htonl(T);
    memcpy(myPckt + RTL_HEAD_LEN, &tOut, 4);
    memcpy(myPckt + RTL_HEAD_LEN + 4, &wndSize, 4);
    memcpy(myPckt + RTL_HEAD_LEN + 8, &p, 1);
    memcpy(myPckt + RTL_HEAD_LEN + 9, &transID, 4);
    // Configure receive timeout for socket (only for this operation).
    struct timeval synTimeO;
    synTimeO.tv_sec = __SYNACK_TIMEO;
    synTimeO.tv_usec = 0;
    struct timeval noTimeO;
    noTimeO.tv_usec = 0;
    noTimeO.tv_sec = 0;
    if (setsockopt(sockFD, SOL_SOCKET, SO_RCVTIMEO, &synTimeO,
                   (socklen_t)sizeof(struct timeval)) == -1) return NULL;
    // Send SYN packet and wait for response.
    socklen_t addrLen = sizeof(struct sockaddr_in);
    if (sendto(sockFD, myPckt, RTL_HEAD_LEN + RTL_SYN_DATA, 0,
               (struct sockaddr *)&addr, addrLen) == -1) return NULL;
    if ((recvfrom(sockFD, synAck, UDP_MAXPLD, 0,
                  (struct sockaddr *)&addr, &addrLen) == -1) ||
        !(synAck[0] & SYN) ||
        !(synAck[0] & ACK)) {
        setsockopt(sockFD, SOL_SOCKET, SO_RCVTIMEO, &noTimeO,
                   (socklen_t)sizeof(struct timeval));
        return NULL;
    }
    // Reset socket receive timeout.
    if (setsockopt(sockFD, SOL_SOCKET, SO_RCVTIMEO, &noTimeO,
                   (socklen_t)sizeof(struct timeval)) == -1) return NULL;
    // Check transaction ID.
    uint32_t retTransID = 0;
    memcpy(&retTransID, synAck + RTL_HEAD_LEN, 4);
    if (retTransID != transID) {
        errno = EBADMSG;
        return NULL;
    }
    // Read server's sequence number.
    uint32_t nextInNumNet = 0;
    memcpy(&nextInNumNet, synAck + SEQNUM_OFFST, 4);
    uint32_t nextInNum = ntohl(nextInNumNet);
    if (nextInNum == maxValue) nextInNum = 0;
    else nextInNum++;
    // Connect our socket.
    if (connect(sockFD, (struct sockaddr *)&addr,
                sizeof(struct sockaddr_in)) != 0)
        return NULL;
    // Initialize argument buffers for I/O threads and return values buffer.
    _inputEntArgs *inputEntArgs = calloc(1, sizeof(_inputEntArgs));
    _outputEntArgs *outputEntArgs = calloc(1, sizeof(_outputEntArgs));
    _chanDmxArgs *chanDmxArgs = calloc(1, sizeof(_chanDmxArgs));
    RTLConnectValues *returnValues = calloc(1, sizeof(RTLConnectValues));
    pthread_mutex_t *outputPipeLock = calloc(1, sizeof(pthread_mutex_t));
    pthread_mutex_t *chanAVLLock = calloc(1, sizeof(pthread_mutex_t));
    pthread_mutex_t *closeLock = calloc(1, sizeof(pthread_mutex_t));
    sem_t *outputPipeSem = calloc(1, sizeof(sem_t));
    sem_t *ioCloseSem = calloc(1, sizeof(sem_t));
    __receiver_wnd_entry *recvWnd = calloc(N, sizeof(__receiver_wnd_entry));
    __sender_wnd_entry *sendWnd = calloc(N, sizeof(__sender_wnd_entry));
    FibHeap *timeHeap = createFibHeap(__INIT_HEAP_ORD);
    if ((inputEntArgs == NULL) || (outputEntArgs == NULL) ||
        (returnValues == NULL) || (chanDmxArgs == NULL) ||
        (outputPipeLock == NULL) || (chanAVLLock == NULL) ||
        (recvWnd == NULL) || (sendWnd == NULL) || (timeHeap == NULL) ||
        (outputPipeSem == NULL) || (closeLock == NULL) ||
        (ioCloseSem == NULL)) {
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        errno = ENOMEM;
        return NULL;
    }
    // Open internal pipes.
    int chanDmxPipe[2], internalInputPipe[2], outputPipe[2], chan0Pipe[2];
    if ((pipe2(chanDmxPipe, O_DIRECT | O_CLOEXEC) == -1) ||
        (pipe2(internalInputPipe, O_DIRECT | O_CLOEXEC) == -1) ||
        (pipe2(outputPipe, O_DIRECT | O_CLOEXEC) == -1) ||
        (pipe2(chan0Pipe, O_CLOEXEC) == -1) ||
        (fcntl(chanDmxPipe[0], F_SETPIPE_SZ, __RTL_PIPESZ) < __RTL_PIPESZ) ||
        (fcntl(internalInputPipe[0], F_SETPIPE_SZ, __RTL_PIPESZ) <
         __RTL_PIPESZ) ||
        (fcntl(chan0Pipe[0], F_SETPIPE_SZ, __RTL_PIPESZ) < __RTL_PIPESZ)) {
        int prevErrno = errno;
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        errno = prevErrno;
        return NULL;
    }
    // Create output timer.
    int timerFD = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
    if (timerFD == -1) {
        int prevErrno = errno;
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        errno = prevErrno;
        eraseFibHeap(timeHeap);
    }
    // Initialize locks.
    if ((pthread_mutex_init(outputPipeLock, NULL) != 0) ||
        (pthread_mutex_init(chanAVLLock, NULL) != 0) ||
        (pthread_mutex_init(closeLock, NULL) != 0) ||
        (sem_init(outputPipeSem, 0, 0) != 0) ||
        (sem_init(ioCloseSem, 0, 0) != 0)) {
        int prevErrno = errno;
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        close(timerFD);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        errno = prevErrno;
        return NULL;
    }
    // Initialize Input Entity structures and data.
    inputEntArgs->sockFD = sockFD;
    inputEntArgs->nextInNum = nextInNum;
    inputEntArgs->windowSize = N;
    inputEntArgs->maxSeqNum = maxValue;
    inputEntArgs->chanDmxPipe_write = chanDmxPipe[1];
    inputEntArgs->inputPipeFD_write = internalInputPipe[1];
    inputEntArgs->_recvWnd = recvWnd;
    inputEntArgs->outputPipeLock = outputPipeLock;
    inputEntArgs->outputPipeFD_write = outputPipe[1];
    inputEntArgs->_outputPipeSem = outputPipeSem;
    inputEntArgs->_closeLock = closeLock;
    // Initialize Output Entity structures and data.
    outputEntArgs->sockFD = sockFD;
    outputEntArgs->tOut = T;
    outputEntArgs->lossProb = p;
    outputEntArgs->windowSize = N;
    outputEntArgs->maxSeqNum = maxValue;
    outputEntArgs->firstOutNum = nextOutNum;
    outputEntArgs->inputPipeFD_read = internalInputPipe[0];
    outputEntArgs->outputPipeFD_read = outputPipe[0];
    outputEntArgs->_sendWnd = sendWnd;
    outputEntArgs->_timeHeap = timeHeap;
    outputEntArgs->timerFD = timerFD;
    outputEntArgs->outputPipeSem = outputPipeSem;
    outputEntArgs->_ioCloseSem = ioCloseSem;
    // Initialize Channel Demux structures and data.
    // Channel AVL Tree will be initialized with an entry for channel 0.
    chanDmxArgs->chanDmxPipe_read = chanDmxPipe[0];
    chanDmxArgs->_chanAVLLock = chanAVLLock;
    chanDmxArgs->_chanAVL = createIntTree();
    returnValues->_chanAVL = chanDmxArgs->_chanAVL;
    if ((chanDmxArgs->_chanAVL == NULL) ||
        (intInsert(chanDmxArgs->_chanAVL, 0, chan0Pipe[1])) != 1) {
        deleteIntTree(chanDmxArgs->_chanAVL);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        pthread_mutex_destroy(outputPipeLock);
        pthread_mutex_destroy(chanAVLLock);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        close(timerFD);
        errno = ENOMEM;
        return NULL;
    }
    // Spawn I/O Entity threads.
    if ((pthread_create(&(inputEntArgs->chanDmxTID), NULL, _chanDmx,
            (void *)chanDmxArgs) != 0) ||
        (pthread_create(&(outputEntArgs->_inputEntTID), NULL, _inputEntity,
            (void *)inputEntArgs) != 0) ||
        (pthread_create(&(returnValues->_outputEntityTID), NULL,
            _outputEntity, (void *)outputEntArgs) != 0)) {
        int prevErrno = errno;
        deleteIntTree(chanDmxArgs->_chanAVL);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        pthread_mutex_destroy(outputPipeLock);
        pthread_mutex_destroy(chanAVLLock);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        close(timerFD);
        errno = prevErrno;
        return NULL;
    }
    // Set return values.
    returnValues->_inputEntityTID = outputEntArgs->_inputEntTID;
    returnValues->chan0PipeFD_read = chan0Pipe[0];
    returnValues->outputPipeLock = outputPipeLock;
    returnValues->outputPipeFD_write = outputPipe[1];
    returnValues->_chanAVLLock = chanAVLLock;
    returnValues->_outputPipeSem = outputPipeSem;
    returnValues->_closeLock = closeLock;
    returnValues->_ioCloseSem = ioCloseSem;
    // Send ACK.
    memset(myPckt, 0, RTL_HEAD_LEN + RTL_SYN_DATA);
    myPckt[0] = ACK;
    memcpy(myPckt + SEQNUM_OFFST, &nextInNumNet, 4);
    if (send(sockFD, myPckt, RTL_HEAD_LEN, 0) == -1) {
        pthread_cancel(returnValues->_inputEntityTID);
        pthread_cancel(returnValues->_outputEntityTID);
        free(returnValues);
        return NULL;
    }
    // Three-way handshake completed! Connection established!
    return returnValues;
}

/* Accepts incoming connection requests.
 * Creates new sockets for clients and connects them.
 * IF FORKING AFTERWARDS: In the parent process, close the original FD.
 */
RTLAcceptValues *rtlAccept(int sock) {

    // Variables initialization.
    char newReq[RTL_HEAD_LEN + RTL_SYN_DATA];
    memset(newReq, 0 , RTL_HEAD_LEN + RTL_SYN_DATA);
    struct sockaddr_in inAddr;
    memset(&inAddr, 0, sizeof(struct sockaddr_in));
    uint32_t N = 0;
    uint32_t initialSeqNum = 0;
    uint32_t maxSeqNum = 0;
    int32_t _T = 0;
    unsigned char _p = 0;
    int newSock = 0;
    uint32_t transID = 0;
    socklen_t addrLen = sizeof(struct sockaddr_in);
    RTLAcceptValues *acceptValues = calloc(1, sizeof(RTLAcceptValues));
    if (acceptValues == NULL) {
        free(acceptValues);
        errno = ENOMEM;
        return NULL;
    }

    // Wait for new connection request.
    if (recvfrom(sock, newReq, RTL_HEAD_LEN + RTL_SYN_DATA, 0, &inAddr,
                 &addrLen) == -1) {
        free(acceptValues);
        return NULL;
    }

    // Fetch values from new connection request packet.
    memcpy(&initialSeqNum, newReq + SEQNUM_OFFST, 4);
    initialSeqNum = ntohl(initialSeqNum);
    memcpy(&_T, newReq + RTL_HEAD_LEN, 4);
    _T = (int32_t)ntohl(_T);
    memcpy(&N, newReq + RTL_HEAD_LEN + 4, 4);
    N = ntohl(N);
    memcpy(&_p, newReq + RTL_HEAD_LEN + 8, 1);
    memcpy(&transID, newReq + RTL_HEAD_LEN + 9, 4);

    // Compute maxSeqNum.
    maxSeqNum = _checkWnd(N);
    // Increase initialSeqNum.
    if (initialSeqNum == maxSeqNum) initialSeqNum = 0;
    else initialSeqNum++;

    // Get your IP address.
    struct sockaddr_in myAddr;
    memset(&myAddr, 0, sizeof(struct sockaddr_in));
    socklen_t myAddrLen = 0;
    if (getsockname(sock, (struct sockaddr *)&myAddr, &myAddrLen) != 0) {
        free(acceptValues);
        return NULL;
    }
    // Create new UDP socket for this client.
    newSock = rtlInit(NULL, 0);  // We use this to set buffers as well.
    if (newSock == RTL_ERROR) {
        int prevErrno = errno;
        free(acceptValues);
        close(newSock);
        errno = prevErrno;
        return NULL;
    }
    // Bind new socket to current interface and new port.
    myAddr.sin_port = 0;
    if (bind(newSock, (struct sockaddr *)&myAddr, sizeof(struct sockaddr_in))
            == -1) {
        int prevErrno = errno;
        free(acceptValues);
        close(newSock);
        errno = prevErrno;
        return NULL;
    }

    // Connect the new socket to the client.
    if (connect(newSock, (struct sockaddr *)&inAddr,
            sizeof(struct sockaddr_in)) != 0) {
        int prevErrno = errno;
        free(acceptValues);
        close(newSock);
        errno = prevErrno;
        return NULL;
    }

    // Create RTLAcceptValues.
    acceptValues->_initialInSeqNum = initialSeqNum;
    acceptValues->_maxSeqNum = maxSeqNum;
    acceptValues->_N = N;
    acceptValues->_T = _T;
    acceptValues->_p = _p;
    acceptValues->newSock = newSock;
    acceptValues->transID = transID;

    return acceptValues;
}

/* Completes the connection, server-side, including the 3-way handshake.
 * Spawns I/O Entities and eventually cleans parent process internal data
 * after a fork (that MUST be done BEFORE this call).
 * WARNING: Erases the accData argument.
 */
RTLConnectValues *rtlAttach(RTLAcceptValues *accData, int oldSock,
        int forking) {
    // If we're in a child process, close the socket used by accept.
    if (forking != 0) close(oldSock);
    // Generate initial sequence number and set maximum sequence number.
    struct drand48_data myRandData;
    memset(&myRandData, 0, sizeof(myRandData));
    srand48_r(getpid(), &myRandData);
    long int randNum = 0;
    lrand48_r(&myRandData, &randNum);
    uint32_t maxValue = accData->_maxSeqNum;
    uint32_t nextOutNum = (uint32_t)randNum % maxValue;
    // Initialize argument buffers for I/O threads and return values buffer.
    _inputEntArgs *inputEntArgs = calloc(1, sizeof(_inputEntArgs));
    _outputEntArgs *outputEntArgs = calloc(1, sizeof(_outputEntArgs));
    _chanDmxArgs *chanDmxArgs = calloc(1, sizeof(_chanDmxArgs));
    RTLConnectValues *returnValues = calloc(1, sizeof(RTLConnectValues));
    pthread_mutex_t *outputPipeLock = calloc(1, sizeof(pthread_mutex_t));
    pthread_mutex_t *chanAVLLock = calloc(1, sizeof(pthread_mutex_t));
    pthread_mutex_t *closeLock = calloc(1, sizeof(pthread_mutex_t));
    sem_t *outputPipeSem = calloc(1, sizeof(sem_t));
    sem_t *ioCloseSem = calloc(1, sizeof(sem_t));
    __receiver_wnd_entry *recvWnd = calloc(accData->_N,
                                           sizeof(__receiver_wnd_entry));
    __sender_wnd_entry *sendWnd = calloc(accData->_N,
                                         sizeof(__sender_wnd_entry));
    FibHeap *timeHeap = createFibHeap(__INIT_HEAP_ORD);
    if ((inputEntArgs == NULL) || (outputEntArgs == NULL) ||
        (returnValues == NULL) || (chanDmxArgs == NULL) ||
        (outputPipeLock == NULL) || (chanAVLLock == NULL) ||
        (recvWnd == NULL) || (sendWnd == NULL) || (timeHeap == NULL) ||
        (outputPipeSem == NULL) || (closeLock == NULL) ||
        (ioCloseSem == NULL)) {
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        errno = ENOMEM;
        return NULL;
    }
    // Open internal pipes.
    int chanDmxPipe[2], internalInputPipe[2], outputPipe[2], chan0Pipe[2];
    if ((pipe2(chanDmxPipe, O_DIRECT | O_CLOEXEC) == -1) ||
        (pipe2(internalInputPipe, O_DIRECT | O_CLOEXEC) == -1) ||
        (pipe2(outputPipe, O_DIRECT | O_CLOEXEC) == -1) ||
        (pipe2(chan0Pipe, O_CLOEXEC) == -1) ||
        (fcntl(chanDmxPipe[0], F_SETPIPE_SZ, __RTL_PIPESZ) < __RTL_PIPESZ) ||
        (fcntl(internalInputPipe[0], F_SETPIPE_SZ, __RTL_PIPESZ)
         < __RTL_PIPESZ) ||
        (fcntl(chan0Pipe[0], F_SETPIPE_SZ, __RTL_PIPESZ) < __RTL_PIPESZ)) {
        int prevErrno = errno;
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        errno = prevErrno;
        return NULL;
    }
    // Create output timer.
    int timerFD = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
    if (timerFD == -1) {
        int prevErrno = errno;
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        errno = prevErrno;
        return NULL;
    }
    // Initialize locks.
    if ((pthread_mutex_init(outputPipeLock, NULL) != 0) ||
        (pthread_mutex_init(chanAVLLock, NULL) != 0) ||
        (pthread_mutex_init(closeLock, NULL) != 0) ||
        (sem_init(outputPipeSem, 0, 0) != 0) ||
        (sem_init(ioCloseSem, 0, 0) != 0)) {
        int prevErrno = errno;
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        close(timerFD);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        errno = prevErrno;
        return NULL;
    }
    // Initialize Input Entity structures and data.
    inputEntArgs->sockFD = accData->newSock;
    inputEntArgs->nextInNum = accData->_initialInSeqNum;
    inputEntArgs->windowSize = accData->_N;
    inputEntArgs->maxSeqNum = maxValue;
    inputEntArgs->chanDmxPipe_write = chanDmxPipe[1];
    inputEntArgs->inputPipeFD_write = internalInputPipe[1];
    inputEntArgs->_recvWnd = recvWnd;
    inputEntArgs->outputPipeLock = outputPipeLock;
    inputEntArgs->outputPipeFD_write = outputPipe[1];
    inputEntArgs->_outputPipeSem = outputPipeSem;
    inputEntArgs->_closeLock = closeLock;
    // Initialize Output Entity structures and data.
    outputEntArgs->sockFD = accData->newSock;
    outputEntArgs->tOut = accData->_T;
    outputEntArgs->lossProb = accData->_p;
    outputEntArgs->windowSize = accData->_N;
    outputEntArgs->maxSeqNum = maxValue;
    outputEntArgs->firstOutNum = nextOutNum;
    outputEntArgs->inputPipeFD_read = internalInputPipe[0];
    outputEntArgs->outputPipeFD_read = outputPipe[0];
    outputEntArgs->_sendWnd = sendWnd;
    outputEntArgs->_timeHeap = timeHeap;
    outputEntArgs->timerFD = timerFD;
    outputEntArgs->outputPipeSem = outputPipeSem;
    outputEntArgs->_ioCloseSem = ioCloseSem;
    // Initialize Channel Demux structures and data.
    // Channel AVL Tree will be initialized with an entry for channel 0.
    chanDmxArgs->chanDmxPipe_read = chanDmxPipe[0];
    chanDmxArgs->_chanAVLLock = chanAVLLock;
    chanDmxArgs->_chanAVL = createIntTree();
    returnValues->_chanAVL = chanDmxArgs->_chanAVL;
    if ((chanDmxArgs->_chanAVL == NULL) ||
        (intInsert(chanDmxArgs->_chanAVL, 0, chan0Pipe[1])) != 1) {
        deleteIntTree(chanDmxArgs->_chanAVL);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        pthread_mutex_destroy(outputPipeLock);
        pthread_mutex_destroy(chanAVLLock);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        close(timerFD);
        errno = ENOMEM;
        return NULL;
    }
    // Spawn I/O Entity threads.
    if ((pthread_create(&(inputEntArgs->chanDmxTID), NULL, _chanDmx,
                        (void *)chanDmxArgs) != 0) ||
        (pthread_create(&(outputEntArgs->_inputEntTID), NULL, _inputEntity,
                        (void *)inputEntArgs) != 0) ||
        (pthread_create(&(returnValues->_outputEntityTID), NULL,
                        _outputEntity, (void *)outputEntArgs) != 0)) {
        int prevErrno = errno;
        deleteIntTree(chanDmxArgs->_chanAVL);
        free(inputEntArgs);
        free(outputEntArgs);
        free(chanDmxArgs);
        free(returnValues);
        pthread_mutex_destroy(outputPipeLock);
        pthread_mutex_destroy(chanAVLLock);
        free(outputPipeLock);
        free(chanAVLLock);
        free(recvWnd);
        free(sendWnd);
        free(outputPipeSem);
        free(closeLock);
        free(ioCloseSem);
        eraseFibHeap(timeHeap);
        close(chanDmxPipe[0]);
        close(chanDmxPipe[1]);
        close(internalInputPipe[0]);
        close(internalInputPipe[1]);
        close(outputPipe[0]);
        close(outputPipe[1]);
        close(chan0Pipe[0]);
        close(chan0Pipe[1]);
        close(timerFD);
        errno = prevErrno;
        return NULL;
    }
    // Set return values.
    returnValues->_inputEntityTID = outputEntArgs->_inputEntTID;
    returnValues->chan0PipeFD_read = chan0Pipe[0];
    returnValues->outputPipeLock = outputPipeLock;
    returnValues->outputPipeFD_write = outputPipe[1];
    returnValues->_chanAVLLock = chanAVLLock;
    returnValues->_outputPipeSem = outputPipeSem;
    returnValues->_closeLock = closeLock;
    returnValues->_ioCloseSem = ioCloseSem;
    // Send SYN_ACK.
    // Note that we can now use the whole underlying architecture!
    char synAck[RTL_HEAD_LEN + 4];
    memset(synAck, 0, RTL_HEAD_LEN + 4);
    synAck[0] = SYN | ACK;
    uint16_t synAckDataLen = 4;
    synAckDataLen = htons(synAckDataLen);
    memcpy(synAck + DATALEN_OFFST, &synAckDataLen, 2);
    memcpy(synAck + RTL_HEAD_LEN, &(accData->transID), 4);
    if (write(outputPipe[1], synAck, RTL_HEAD_LEN + 4) == -1) {
        int prevErrno = errno;
        pthread_cancel(returnValues->_inputEntityTID);
        pthread_cancel(returnValues->_outputEntityTID);
        free(returnValues);
        free(accData);
        errno = prevErrno;
        return NULL;
    }
    sem_wait(outputPipeSem);
    // Three-way handshake completed! Connection established.
    free(accData);
    return returnValues;
}

/* Closes a client connection, freeing resources.
 * WARNING: Must be called after ALL I/O operations on that connection have
 * been terminated, but doesn't require to verify that all data has been sent.
 * All further attempts to send data will result in a deadlock.
 * All further attempts to receive data will result in I/O errors.
 * The connData argument is erased.
 * All channels are irreversibly closed without acquiring locks first.
 */
int rtlClose(RTLConnectValues *connData) {
    // Try to acquire closing procedure lock.
    if (pthread_mutex_trylock(connData->_closeLock) == 0) {
        // Initiate closing procedure.
        // Irreversibly lock output pipe.
        pthread_mutex_lock(connData->outputPipeLock);
        // Transmit FIN packet.
        char fin[RTL_HEAD_LEN];
        memset(fin, 0, RTL_HEAD_LEN);
        fin[0] = FIN;
        if (write(connData->outputPipeFD_write, fin, RTL_HEAD_LEN) == -1)
            kill(getpid(), SIGPIPE);
        sem_wait(connData->_outputPipeSem);
    }
    // Wait for I/O Entities to terminate.
    pthread_join(connData->_inputEntityTID, NULL);
    pthread_join(connData->_outputEntityTID, NULL);
    // Destroy connection values and release memory.
    close(connData->outputPipeFD_write);
    close(connData->chan0PipeFD_read);
    pthread_mutex_destroy(connData->_closeLock);
    pthread_mutex_destroy(connData->outputPipeLock);
    free(connData->_closeLock);
    free(connData->outputPipeLock);
    sem_destroy(connData->_ioCloseSem);
    free(connData->_ioCloseSem);
    free(connData);
    return 0;
}
