cmake_minimum_required(VERSION 3.14)

project(filetransmissionpinno99 C)

# CMake settings.
set(CMAKE_C_STANDARD 99)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# Compiler settings.
set(CMAKE_CXX_FLAGS_DEBUG "-Wall -Wextra -O0")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

# ARM cross-compilation settings.
if ((CMAKE_BUILD_TYPE MATCHES RPi-Debug) OR (CMAKE_BUILD_TYPE MATCHES
        RPi-Release))
    set(CMAKE_FIND_ROOT_PATH /home/lhrobs/raspberrypi/x-tools-aarch64/)
    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    include_directories(/home/lhrobs/raspberrypi/rootfs-gentoo/usr/lib/aarch64-unknown-linux-gnu/9.2.0/include/)
endif()
if (CMAKE_BUILD_TYPE MATCHES RPi-Release)
    set(CMAKE_CXX_FLAGS "-O3")
endif()

# Testing executables.
add_executable(lsTest Testers/lsTest.c)
add_executable(cloneTest Testers/cloneTest.c)
add_executable(cancelTest Testers/cancelTest.c)
target_link_libraries(cancelTest PRIVATE pthread)
add_executable(heapTester Testers/heapTester.c
        Libraries/DoubleLinkedList/doubleLinkedList.h
        Libraries/DoubleLinkedList/doubleLinkedList.c
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.c)
add_executable(AVLTree_IK_Tester Testers/AVL_IK_Tester.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c)
add_executable(AVLTree_IK_HeavyTester Testers/AVLTree_IK_HeavyTester.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c)
add_executable(timerTest Testers/timerTest.c)
add_executable(rtl_echoServer Testers/rtl_echoServer.c
        librtl/rtl.h
        librtl/librtl_definitions.h
        librtl/librtl_control.c
        librtl/librtl_comms.c
        librtl/librtl_threads.c
        Libraries/DoubleLinkedList/doubleLinkedList.h
        Libraries/DoubleLinkedList/doubleLinkedList.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.c)
target_link_libraries(rtl_echoServer PRIVATE pthread)
add_executable(rtl_echoClient Testers/rtl_echoClient.c
        librtl/rtl.h
        librtl/librtl_definitions.h
        librtl/librtl_control.c
        librtl/librtl_comms.c
        librtl/librtl_threads.c
        Libraries/DoubleLinkedList/doubleLinkedList.h
        Libraries/DoubleLinkedList/doubleLinkedList.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.c)
target_link_libraries(rtl_echoClient PRIVATE pthread)
add_executable(rtl_fileEchoServer Testers/rtl_fileEchoServer.c
        librtl/rtl.h
        librtl/librtl_definitions.h
        librtl/librtl_control.c
        librtl/librtl_comms.c
        librtl/librtl_threads.c
        Libraries/DoubleLinkedList/doubleLinkedList.h
        Libraries/DoubleLinkedList/doubleLinkedList.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.c)
target_link_libraries(rtl_fileEchoServer PRIVATE pthread)
add_executable(rtl_fileEchoClient Testers/rtl_fileEchoClient.c
        librtl/rtl.h
        librtl/librtl_definitions.h
        librtl/librtl_control.c
        librtl/librtl_comms.c
        librtl/librtl_threads.c
        Libraries/DoubleLinkedList/doubleLinkedList.h
        Libraries/DoubleLinkedList/doubleLinkedList.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.c)
target_link_libraries(rtl_fileEchoClient PRIVATE pthread)

# FTP99 Server.
add_executable(ftp99_server Server/serverMain.c Server/serverAux.c
        Server/serverThreads.c ftp99_threads.c
        FTP99.h
        librtl/rtl.h
        librtl/librtl_definitions.h
        librtl/librtl_control.c
        librtl/librtl_comms.c
        librtl/librtl_threads.c
        Libraries/DoubleLinkedList/doubleLinkedList.h
        Libraries/DoubleLinkedList/doubleLinkedList.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.c)
target_link_libraries(ftp99_server PRIVATE pthread)

# FTP99 Client.
add_executable(ftp99_client Client/clientMain.c ftp99_threads.c
        FTP99.h
        librtl/rtl.h
        librtl/librtl_definitions.h
        librtl/librtl_control.c
        librtl/librtl_comms.c
        librtl/librtl_threads.c
        Libraries/DoubleLinkedList/doubleLinkedList.h
        Libraries/DoubleLinkedList/doubleLinkedList.c
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h
        Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.c
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h
        Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.c)
target_link_libraries(ftp99_client PRIVATE pthread)

# Debugging enhancements.
if ((CMAKE_BUILD_TYPE MATCHES Debug) OR (CMAKE_BUILD_TYPE MATCHES RPi-Debug))
    # Keep preprocessor and compiler (assembly) output.
    set_target_properties(heapTester PROPERTIES COMPILE_FLAGS "-save-temps")
    set_target_properties(AVLTree_IK_Tester PROPERTIES COMPILE_FLAGS
            "-save-temps")
    set_target_properties(AVLTree_IK_HeavyTester PROPERTIES COMPILE_FLAGS
            "-save-temps")
    set_target_properties(timerTest PROPERTIES COMPILE_FLAGS "-save-temps")
    set_target_properties(rtl_echoServer PROPERTIES COMPILE_FLAGS "-save-temps")
    set_target_properties(rtl_echoClient PROPERTIES COMPILE_FLAGS "-save-temps")
    set_target_properties(rtl_fileEchoServer PROPERTIES COMPILE_FLAGS
            "-save-temps")
    set_target_properties(rtl_fileEchoClient PROPERTIES COMPILE_FLAGS
            "-save-temps")
endif()
