/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 28/8/2019
 * -----------------------------------------------------------------------------
 * Echo server to test the RTL library.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../librtl/rtl.h"

#include "addressing.h"

#define MSGSIZE 1500

int main(void) {
    // Initialize RTL.
    int sockFD = rtlInit(SERVER_IP, SERVER_PORT);
    if (sockFD == RTL_ERROR) {
        fprintf(stderr, "ERROR: Failed to initialize RTL.\n");
        exit(EXIT_FAILURE);
    }
    // Accept a new connection.
    RTLAcceptValues *accData = rtlAccept(sockFD);
    if (accData == NULL) {
        fprintf(stderr, "ERROR: Failed to accept a new connection.\n");
        exit(EXIT_FAILURE);
    }
    // Attach the new client.
    RTLConnectValues *connData = rtlAttach(accData, sockFD, 0);
    if (connData == NULL) {
        fprintf(stderr, "ERROR: Failed to attach new client.\n");
        exit(EXIT_FAILURE);
    }
    printf("Connection established!\n");
    char myMsg[MSGSIZE + 3];
    ssize_t recvBytes, recvRes;
    size_t msgLen;
    // Receive message and send it back.
    for (;;) {
        recvBytes = 0;
        memset(myMsg, 0, MSGSIZE + 3);
        do {
            recvRes = rtlRecv(0, 0, connData, myMsg + recvBytes,
                    MSGSIZE - recvBytes);
            if (recvRes == RTL_ERROR) {
                fprintf(stderr, "ERROR: Failed to receive message.\n");
                exit(EXIT_FAILURE);
            }
            recvBytes += recvRes;
        } while (strstr(myMsg, "\r\n") == NULL);
        msgLen = strlen(myMsg);
        printf("Got %lu bytes, sending back...\n", msgLen);
        if (rtlSend(0, connData, myMsg, msgLen) != msgLen) {
            fprintf(stderr, "ERROR: Failed to send message.\n");
            exit(EXIT_FAILURE);
        }
    }
    return 0;
}
