/* Roberto Masocco
 * Creation Date: 26/7/2019
 * Latest Version: 26/7/2019
 * ----------------------------------------------------------------------------
 * This is the source file of a tester application for the AVL Tree data
 * structure.
 * This test stores long integers as data, heavily using the heap.
 */

#include <stdio.h>
#include <stdlib.h>
#include "../Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h"

#define fflush(stdin) while (getchar() != '\n')

/* Subroutine to print search results. */
void printSearch(int *keys, int *data, unsigned long int len) {
    if ((keys == NULL) || (data == NULL)) {
        // Sanity check.
        printf("Nothing!\n");
        return;
    }
    putchar('[');
    for (int i = 0; i < len; i++) {
        printf("(%d, %d), ", keys[i], data[i]);
    }
    printf("\b\b]\n");
    free(keys);
    free(data);
}

/* The works. */
int main(void) {
    int nodesNum, newData, *inOPtr, *preOPtr, *postOPtr, *bfsLPtr,
    *bfsRPtr;
    int keyPtr, *inOKeys, *preOKeys, *postOKeys, *bfsLKeys, *bfsRKeys;
    char ans;
    AVLIntTree *tree = createIntTree();
    if (tree == NULL) {
        fprintf(stderr, "ERROR: Failed to create the tree.\n");
        exit(EXIT_FAILURE);
    }
    printf("This test for the AVL Tree (with integers as keys) data structure "
           "uses integers as data.\n");
    printf("Enter how many nodes you want to add: ");
    if (scanf("%d", &nodesNum) != 1) {
        fprintf(stderr, "ERROR: Invalid input.\n");
        exit(EXIT_FAILURE);
    }
    fflush(stdin);
    for (int i = 0; i < nodesNum; i++) {
        printf("Enter the key for node #%d: ", i + 1);
        if (scanf("%d", &keyPtr) != 1) {
            fprintf(stderr, "ERROR: Invalid input.\n");
            exit(EXIT_FAILURE);
        }
        fflush(stdin);
        printf("Enter an integer for node #%d: ", i + 1);
        if (scanf("%d", &newData) != 1) {
            fprintf(stderr, "ERROR: Invalid input.\n");
            exit(EXIT_FAILURE);
        }
        fflush(stdin);
        printf("Inserting node #%d...", i + 1);
        printf(" \"insert\" returned %ld.\n", intInsert(tree, keyPtr, newData));
        putchar('\n');
    }
    printf("Insertions done. ");
    printf("The tree contains %lu node(s),", tree->nodesCount);
    printf(" up to a maximum of %lu.\n", tree->maxNodes);
    printf("Starting searches...\n");
    inOPtr = (int *)intDFS(tree, DFS_IN_ORDER, SEARCH_DATA);
    preOPtr = (int *)intDFS(tree, DFS_PRE_ORDER, SEARCH_DATA);
    postOPtr = (int *)intDFS(tree, DFS_POST_ORDER, SEARCH_DATA);
    inOKeys = (int *)intDFS(tree, DFS_IN_ORDER, SEARCH_KEYS);
    preOKeys = (int *)intDFS(tree, DFS_PRE_ORDER, SEARCH_KEYS);
    postOKeys = (int *)intDFS(tree, DFS_POST_ORDER, SEARCH_KEYS);
    bfsLPtr = (int *)intBFS(tree, BFS_LEFT_FIRST, SEARCH_DATA);
    bfsLKeys = (int *)intBFS(tree, BFS_LEFT_FIRST, SEARCH_KEYS);
    bfsRPtr = (int *)intBFS(tree, BFS_RIGHT_FIRST, SEARCH_DATA);
    bfsRKeys = (int *)intBFS(tree, BFS_RIGHT_FIRST, SEARCH_KEYS);
    printf("In-order DFSs returned:\n");
    printSearch(inOKeys, inOPtr, tree->nodesCount);
    printf("Pre-order DFSs returned:\n");
    printSearch(preOKeys, preOPtr, tree->nodesCount);
    printf("Post-order DFSs returned:\n");
    printSearch(postOKeys, postOPtr, tree->nodesCount);
    printf("Left-first BFSs returned:\n");
    printSearch(bfsLKeys, bfsLPtr, tree->nodesCount);
    printf("Right-first BFSs returned:\n");
    printSearch(bfsRKeys, bfsRPtr, tree->nodesCount);
    for (;;) {
        printf("Do you want to search for a key? (y/n) ");
        if (scanf("%c", &ans) != 1) {
            fprintf(stderr, "ERROR: Invalid input.\n");
            exit(EXIT_FAILURE);
        }
        fflush(stdin);
        if ((ans == 'y') || (ans == 'Y')) {
            printf("Enter the key to search: ");
            if (scanf("%d", &keyPtr) != 1) {
                fprintf(stderr, "ERROR: Invalid input.\n");
                exit(EXIT_FAILURE);
            }
            fflush(stdin);
            int searchRes = intSearchData(tree, keyPtr);
            if (searchRes == -1) {
                printf("No such key!\n");
                continue;
            } else {
                printf("This key corresponds to: %d.\n", searchRes);
                continue;
            }
        } else if ((ans == 'n') || (ans == 'N')) {
            break;
        } else {
            fprintf(stderr, "ERROR: Invalid input.\n");
            exit(EXIT_FAILURE);
        }
    }
    for (;;) {
        printf("Do you want to delete an element? (y/n) ");
        if (scanf("%c", &ans) != 1) {
            fprintf(stderr, "ERROR: Invalid input.\n");
            exit(EXIT_FAILURE);
        }
        fflush(stdin);
        if ((ans == 'y') || (ans == 'Y')) {
            printf("Enter the key to remove: ");
            if (scanf("%d", &keyPtr) != 1) {
                fprintf(stderr, "ERROR: Invalid input.\n");
                exit(EXIT_FAILURE);
            }
            fflush(stdin);
            if (!intDelete(tree, keyPtr)) {
                printf("Key not found!\n");
                continue;
            } else {
                printf("Element removed. The tree has now %lu node(s).\n",
                        tree->nodesCount);
                printf("Starting in-order DFS...\n");
                inOPtr = (int *)intDFS(tree, DFS_IN_ORDER, SEARCH_DATA);
                inOKeys = (int *)intDFS(tree, DFS_IN_ORDER, SEARCH_KEYS);
                printf("In-order DFS returned:\n");
                printSearch(inOKeys, inOPtr, tree->nodesCount);
                printf("Starting pre-order DFS...\n");
                preOPtr = (int *)intDFS(tree, DFS_PRE_ORDER, SEARCH_DATA);
                preOKeys = (int *)intDFS(tree, DFS_PRE_ORDER, SEARCH_KEYS);
                printf("Pre-order DFS returned:\n");
                printSearch(preOKeys, preOPtr, tree->nodesCount);
                continue;
            }
        } else if ((ans == 'n') || (ans == 'N')) {
            break;
        } else {
            fprintf(stderr, "ERROR: Invalid input.\n");
            exit(EXIT_FAILURE);
        }
    }
    printf("Deleting the tree... got %d from \"deleteIntTree\".\n",
            deleteIntTree(tree));
    printf("Done!\n");
    exit(EXIT_SUCCESS);
}
