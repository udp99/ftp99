#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <errno.h>

#define _RTL_CLIENTCONTROL_SEM "/rtl_clientSem"

sem_t *_clientControlSem = NULL;
pid_t son;

int main(void) {
    _clientControlSem = sem_open(_RTL_CLIENTCONTROL_SEM,
                                 O_CREAT | O_CLOEXEC | O_EXCL | O_RDWR,
                                 S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, 1);
    if ((_clientControlSem == SEM_FAILED) && (errno == EEXIST)) {
        sem_unlink(_RTL_CLIENTCONTROL_SEM);
        _clientControlSem = sem_open(_RTL_CLIENTCONTROL_SEM,
                                     O_CREAT | O_CLOEXEC | O_EXCL | O_RDWR,
                                     S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, 1);
    }
    if (_clientControlSem == SEM_FAILED) exit(EXIT_FAILURE);
    printf("Semaphore opened.\n");
    son = fork();
    if (son < 0) exit(EXIT_FAILURE);
    if (son == 0) {
        sem_wait(_clientControlSem);
        printf("Semaphore acquired!\n");
        exit(EXIT_SUCCESS);
    } else {
        int exitStatus = 0;
        wait(&exitStatus);
        sem_unlink(_RTL_CLIENTCONTROL_SEM);
        printf("Done!\n");
        exit(EXIT_SUCCESS);
    }
    return 0;
}
