/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 28/8/2019
 * -----------------------------------------------------------------------------
 * Echo client to test the RTL library.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../librtl/rtl.h"

#include "addressing.h"

#define MSGSIZE 1500

#define fflush(stdin) while (getchar() != '\n')

int main(void) {
    // Ask for configuration values.
    unsigned int lossProb;
    unsigned int N;
    int T;
    printf("Enter loss probability [0, 99]: ");
    scanf("%u", &lossProb);
    fflush(stdin);
    printf("Enter window size: ");
    scanf("%u", &N);
    fflush(stdin);
    printf("Enter timeout (-1 for TCP-like): ");
    scanf("%d", &T);
    fflush(stdin);
    // Initialize RTL.
    int sockFD = rtlInit(NULL, 0);
    if (sockFD <= 0) {
        fprintf(stderr, "ERROR: Failed to initialize RTL.\n");
        exit(EXIT_FAILURE);
    }
    // Connect to server.
    RTLConnectValues *connData = rtlConnect(SERVER_IP, SERVER_PORT, sockFD, N,
                                            T, (unsigned char)lossProb);
    if (connData == NULL) {
        fprintf(stderr, "ERROR: Failed to connect to server.\n");
        exit(EXIT_FAILURE);
    }
    printf("Connection to server established!\n");
    char myMsg[MSGSIZE + 3];
    ssize_t recvBytes, recvRes;
    // Read from stdin, send, receive, print on stdout.
    for (;;) {
        recvBytes = 0;
        memset(myMsg, 0, MSGSIZE + 3);
        printf("Enter a message: ");
        scanf("%[^\n]", myMsg);
        fflush(stdin);
        memcpy(myMsg + strlen(myMsg), "\r\n", 2);
        ssize_t msgLen = strlen(myMsg);
        if (rtlSend(0, connData, myMsg, msgLen) != msgLen) {
            fprintf(stderr, "ERROR: Failed to send message.\n");
            exit(EXIT_FAILURE);
        }
        memset(myMsg, 0, MSGSIZE + 3);
        do {
            recvRes = rtlRecv(0, 0, connData, myMsg + recvBytes,
                    msgLen - recvBytes);
            if (recvRes == RTL_ERROR) {
                fprintf(stderr, "ERROR: Failed to receive message.\n");
                exit(EXIT_FAILURE);
            }
            recvBytes += recvRes;
        } while (recvBytes < msgLen);
        printf("Got back: %s", myMsg);
    }
    return 0;
}
