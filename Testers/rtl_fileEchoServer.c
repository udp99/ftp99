/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 30/8/2019
 * -----------------------------------------------------------------------------
 * Tester application for the RTL library. Transfers a file in both ways
 * between a client and a server.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../librtl/rtl.h"

#include "addressing.h"

#if __BIG_ENDIAN__
#define htonll(x) (x)
#define ntohll(x) (x)
#else
#define htonll(x) ((uint64_t)htonl((x) & 0xFFFFFFFF) << 32) | htonl((x) >> 32)
#define ntohll(x) ((uint64_t)ntohl((x) & 0xFFFFFFFF) << 32) | ntohl((x) >> 32)
#endif

#define READ_BUFSIZE 4000000
#define WRITE_BUFSIZE 500000

int main(void) {
    pid_t pid;


    // Initialize RTL.
    int sockFD = rtlInit(SERVER_IP, SERVER_PORT);
    if (sockFD == RTL_ERROR) {
        fprintf(stderr, "ERROR: Failed to initialize RTL.\n");
        exit(EXIT_FAILURE);
    }
    for (;;) {

        // Accept a new connection.
        RTLAcceptValues *accData = rtlAccept(sockFD);
        if (accData == NULL) {
            fprintf(stderr, "ERROR: Failed to accept a new connection.\n");
            exit(EXIT_FAILURE);
        }

        pid = fork();

        if (pid == 0) {

            pid_t sonPid = getpid();
            char fileName[10];
            sprintf(fileName, "%ld", sonPid);


            // Initialize buffers.
            char *buf2 = malloc(READ_BUFSIZE);
            char *buf = malloc(WRITE_BUFSIZE);

            // Attach the new client.
            RTLConnectValues *connData = rtlAttach(accData, sockFD, 1);
            if (connData == NULL) {
                fprintf(stderr, "ERROR: Failed to attach new client.\n");
                exit(EXIT_FAILURE);
            }
            printf("Connection established!\n");
            // Receive file size.
            uint64_t fileLen = 0;
            if (rtlRecv(0, 0, connData, (char *) &fileLen, 8) == RTL_ERROR) {
                fprintf(stderr, "ERROR: Failed to receive file size.\n");
                exit(EXIT_FAILURE);
            }
            fileLen = ntohll(fileLen);
            printf("Going to read %lu bytes.\n", fileLen);
            // Open new channel.
            int chan1FD = rtlOpenChannel(connData, 1);
            if (chan1FD == RTL_ERROR) {
                fprintf(stderr, "ERROR: Failed to open new channel.\n");
                exit(EXIT_FAILURE);
            }
            // Read new file.
            FILE *outFile = fopen(fileName, "w+");
            if (outFile == NULL) {
                fprintf(stderr, "ERROR: Failed to open new file.\n");
                exit(EXIT_FAILURE);
            }
            printf("New file: %s\n", fileName);
            uint64_t recvBytes = 0;
            ssize_t recvRes;
            do {
                memset(buf, 0, WRITE_BUFSIZE);
                recvRes = rtlRecv(1, chan1FD, NULL, buf, WRITE_BUFSIZE);
                if (recvRes <= 0) {
                    fprintf(stderr, "ERROR: rtlRecv failed.\n");
                    exit(EXIT_FAILURE);
                }
                if (fwrite(buf, 1, recvRes, outFile) != recvRes) {
                    fprintf(stderr, "ERROR: Failed to write to file.\n");
                    exit(EXIT_FAILURE);
                }
                recvBytes += recvRes;
            } while (recvBytes < fileLen);
            // Send the file backwards.
            printf("Got the file, sending it back...\n");
            rewind(outFile);
            uint64_t sentBytes = 0;
            ssize_t sendRes;
            size_t readRes;
            do {
                memset(buf2, 0, READ_BUFSIZE);
                readRes = fread(buf2, 1, READ_BUFSIZE, outFile);
                if (readRes <= 0) {
                    fprintf(stderr, "ERROR: Failed to read from file.\n");
                    exit(EXIT_FAILURE);
                }
                sendRes = rtlSend(1, connData, buf2, readRes);
                if (sendRes != readRes) {
                    fprintf(stderr, "ERROR: Read %lu bytes, delivered %lu.\n",
                            readRes, sendRes);
                    exit(EXIT_FAILURE);
                }
                sentBytes += sendRes;
            } while (sentBytes < fileLen);
            fclose(outFile);
            printf("File sent back!\n");
            // Close channel.
            if (rtlCloseChannel(1, chan1FD, connData) < 0) {
                fprintf(stderr, "ERROR: Failed to close new channel.\n");
                exit(EXIT_FAILURE);
            }
            printf("Done!\n");
            // Close connection.
            if (rtlClose(connData) != 0) {
                fprintf(stderr, "ERROR: Failed to close connection.\n");
                exit(EXIT_FAILURE);
            }
            printf("Connection closed!\n");
            free(buf);
            free(buf2);
            exit(EXIT_SUCCESS);
        }
    }
}
