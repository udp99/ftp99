/* Use command "ls -laR --color=never [PATH]". */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
	char *lsOut;
	fscanf(stdin, "%m[^]]", &lsOut);
	printf("I read %lu chars:\n%s", strlen(lsOut), lsOut);
	free(lsOut);
	exit(EXIT_SUCCESS);
}
