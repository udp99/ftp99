#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/timerfd.h>

struct itimerspec timerVal;
uint64_t timerCount = 0;

int main(void) {
    timerVal.it_interval.tv_sec = 0;
    timerVal.it_interval.tv_nsec = 0;
    timerVal.it_value.tv_sec = 1;
    timerVal.it_value.tv_nsec = 0;
    int timer = timerfd_create(CLOCK_BOOTTIME, TFD_CLOEXEC);
    if (timer == -1) {
        fprintf(stderr, "ERROR: Failed to create timer.\n");
        perror("timerfd_create");
        exit(EXIT_FAILURE);
    }
    if (timerfd_settime(timer, TFD_TIMER_ABSTIME, &timerVal, NULL) == -1) {
        fprintf(stderr, "ERROR: Failed to set timer.\n");
        perror("timerfd_settime");
        exit(EXIT_FAILURE);
    }
    if (read(timer, &timerCount, 8) == -1) {
        fprintf(stderr, "ERROR: Failed to read from timer's FD.\n");
        perror("read");
        exit(EXIT_FAILURE);
    }
    printf("Read from timer's FD: %lu.\n", timerCount);
    exit(EXIT_SUCCESS);
}
