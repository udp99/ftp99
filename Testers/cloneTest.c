/* Roberto Masocco
 * 31/7/2019
 * -----------------------------------------------------------------------------
 * Little test program to create a child process using "clone" instead of "fork"
 * or "vfork".
 * Only the file descriptors table should be copied, the rest won't be touched.
 * The child process redirects its output on a pipe set up by the parent,
 * then executes our version of ls.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sched.h>
#include <signal.h>

#define CSTACK_SIZE 4096

int childRoutine(void *arg);

int pipefd[2];
char outputBuf[256];

/* The works. */
int main(void) {
    printf("Parent process here, allocating stack...\n");
    void *childStack = malloc(CSTACK_SIZE);
    if (childStack == NULL) {
        fprintf(stderr, "ERROR: Failed to allocate child stack.\n");
        exit(EXIT_FAILURE);
    }
    childStack += CSTACK_SIZE;  // The stack grows downward!
    printf("Opening pipe...\n");
    if (pipe(pipefd) == -1) {
        fprintf(stderr, "ERROR: pipe failed.\n");
        perror("pipe");
        exit(EXIT_FAILURE);
    }
    memset(outputBuf, 0, sizeof outputBuf);
    pid_t childPID;
    printf("Cloning...\n\n");
    // This is the moment of truth guys!
    if (clone(childRoutine, childStack,
              SIGCHLD | CLONE_IO | CLONE_PARENT_SETTID | CLONE_VM,
              NULL, &childPID, NULL, NULL) == -1) {
        fprintf(stderr, "ERROR: clone failed.\n");
        perror("clone");
        fputc('\n', stderr);
        exit(EXIT_FAILURE);
    }
    // We shall now read here as the server appl. operator would: in chunks!
    close(pipefd[1]);
    while (read(pipefd[0], outputBuf, (sizeof outputBuf) - 1)) {
        printf("%s", outputBuf);
        memset(outputBuf, 0, sizeof outputBuf);
        // With this, you'll see the parent emptying the pipe.
        // Useful to simulate chunked I/O with locks, mutexes and shit.
        sleep(1);
    }
    pid_t termPID = wait(NULL);
    if ((termPID == -1) || (termPID != childPID)) {
        fprintf(stderr, "ERROR: wait failed. PID returned: %d.\n", termPID);
        perror("wait");
        exit(EXIT_FAILURE);
    }
    close(pipefd[0]);
    printf("\nGOT HIM!\nTest completed!\n");
    exit(EXIT_SUCCESS);
}

/* Child process routine. Launches ls. */
int childRoutine(void *arg) {
    // Note how everything will take place in the stack, which we promptly
    // allocated for the child.
    (void)arg;
    char *pwd = getenv("PWD");
    if (pwd == NULL) {
        fprintf(stderr, "ERROR: Child failed to retrieve PWD.\n");
        exit(EXIT_FAILURE);
    }
    if (dup2(pipefd[1], STDOUT_FILENO) == -1) exit(EXIT_FAILURE);
    execlp("ls", "ls", "-laR", "--color=never", pwd, NULL);
    // The following code is executed only if execlp fails.
    fprintf(stderr, "ERROR: Failed to execute ls.\n");
    perror("execlp");
    fputc('\n', stderr);
    exit(EXIT_FAILURE);
}
