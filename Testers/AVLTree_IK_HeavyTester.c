/* Roberto Masocco
 * Creation Date: 26/7/2019
 * Latest Version: 26/7/2019
 * ----------------------------------------------------------------------------
 * This file contains the source code for a tester application for the AVL Tree
 * data structure, which uses high amounts of randomly generated keys and
 * data to test the stability of the structure.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h"

#define fflush(stdin) while (getchar() != '\n')

int *randData;
int *randKeys;
int *preODFS, *postODFS, *inODFS, *bfsL, *bfsR;

int _intBalanceFactor(AVLIntNode *node);

/* Subroutine to randomly generate and insert entries. */
void randomNodesGenerator(AVLIntTree *tree, unsigned int nodes) {
    for (unsigned int i = 0; i < nodes; i++) {
        randData[i] = rand();
        randKeys[i] = random() % 30;
        intInsert(tree, randKeys[i], randData[i]);
    }
}

/* The works. */
int main(void) {
    unsigned int insCount, toDelete;
    AVLIntTree *tree = createIntTree();
    if (tree == NULL) {
        fprintf(stderr, "ERROR: Failed to create tree.\n");
        exit(EXIT_FAILURE);
    }
    printf("Enter the number of insertions to do: ");
    if ((scanf("%u", &insCount) != 1) || (insCount == 0)) {
        fprintf(stderr, "ERROR: Invalid input.\n");
        exit(EXIT_FAILURE);
    }
    fflush(stdin);
    srand((unsigned int)getpid());
    randData = (int *)malloc(insCount * sizeof(int));
    randKeys = (int *)malloc(insCount * sizeof(int));
    if ((randKeys == NULL) || (randData == NULL)) {
        fprintf(stderr, "ERROR: Failed to allocate memory for data.\n");
        exit(EXIT_FAILURE);
    }
    randomNodesGenerator(tree, insCount);
    printf("The tree contains now %lu node(s).\n", tree->nodesCount);
    printf("Balance factor at the root is: %d.\n", _intBalanceFactor(tree->_root));
    printf("Press ENTER to start visits...");
    getchar();
    preODFS = (int *)intDFS(tree, DFS_PRE_ORDER, SEARCH_DATA);
    inODFS = (int *)intDFS(tree, DFS_IN_ORDER, SEARCH_DATA);
    postODFS = (int *)intDFS(tree, DFS_POST_ORDER, SEARCH_DATA);
    bfsL = (int *)intBFS(tree, BFS_LEFT_FIRST, SEARCH_DATA);
    bfsR = (int *)intBFS(tree, BFS_RIGHT_FIRST, SEARCH_DATA);
    if ((preODFS == NULL) || (inODFS == NULL) || (postODFS == NULL) ||
        (bfsL == NULL) || (bfsR == NULL)) {
        fprintf(stderr, "ERROR: Visits failed.\n");
        exit(EXIT_FAILURE);
    } else printf("Visits succeeded.\n");
    free(preODFS);
    free(inODFS);
    free(postODFS);
    free(bfsL);
    free(bfsR);
    printf("Enter the number of nodes to delete: ");
    if ((scanf("%u", &toDelete) != 1) || (toDelete > tree->nodesCount)) {
        fprintf(stderr, "ERROR: Invalid input.\n");
        exit(EXIT_FAILURE);
    }
    fflush(stdin);
    printf("Press ENTER to start the deletions...");
    getchar();
    for (int i = 0; i < toDelete; i++) {
        if (!intDelete(tree, randKeys[i])) {
            fprintf(stderr, "ERROR: Failed to delete node #%d.", i + 1);
            fprintf(stderr, " Key was: \"%s\".\n", randKeys[i]);
            exit(EXIT_FAILURE);
        }
    }
    printf("The tree contains now %lu node(s).\n", tree->nodesCount);
    printf("Balance factor at the root is: %d.\n", _intBalanceFactor(tree->_root));
    printf("Press ENTER to start deleting the tree...");
    getchar();
    printf("Got %d from \"deleteIntTree\".\n",
            deleteIntTree(tree));
    free(randData);
    free(randKeys);
    printf("Done!\n");
    exit(EXIT_SUCCESS);
}
