/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 21/8/2019
 * -----------------------------------------------------------------------------
 * Tester program for the Fibonacci Heap library.
 * Offers an interactive part and a performance evaluation test.
 * Output can be redirected to a file (see last part) to check correctness.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>

#define MAX_INIT_ORD 31
#define NODES 1000000UL
#define INCREASES 100000UL
#define DECREASES 100000UL
#define DELETIONS 100000UL

#define fflush(stdin) while (getchar() != '\n')

#include "../Libraries/FibonacciHeap_uint64-keys/FibonacciHeap_uint64-keys.h"

int main(void) {
    FibHeap *heap = createFibHeap(MAX_INIT_ORD);
    if (heap == NULL) {
        fprintf(stderr, "ERROR: Failed to create Binomial Heap.\n");
        exit(EXIT_FAILURE);
    }
    char ans = 0;
    // INTERACTIVE TEST
    printf("Want to do the interactive test? [y/n] ");
    scanf("%c", &ans);
    fflush(stdin);
    if (ans == 'n') {
        goto performance;
    } else if ((ans != 'y') && (ans != 'n')) {
        fprintf(stderr, "WTF\n");
        exit(EXIT_FAILURE);
    }
    uint64_t newKey;
    uint32_t newVal;
    FibTreeNode *insertRes;
    // insert test.
    for (;;) {
        printf("Insert a new key: ");
        scanf("%lu", &newKey);
        fflush(stdin);
        printf("Insert a new value: ");
        scanf("%u", &newVal);
        fflush(stdin);
        insertRes = fhInsert(heap, newVal, newKey);
        if (insertRes == NULL) {
            fprintf(stderr, "ERROR: Failed to insert node.\n");
            exit(EXIT_FAILURE);
        }
        printf("Want to insert another node? [y/n] ");
        scanf("%c", &ans);
        fflush(stdin);
        if (ans == 'y') {
            continue;
        } else if (ans == 'n') {
            break;
        } else {
            fprintf(stderr, "WTF\n");
            exit(EXIT_FAILURE);
        }
    }
    // finMin test.
    uint32_t findMinRes = fhFindMin(heap);
    printf("Element with minimum key is: %u.\n", findMinRes);
    // fhIncreaseKey test.
    FibTreeNode *incNode = heap->min;
    for (;;) {
        printf("Want to increase the minimum key? [y/n] ");
        scanf("%c", &ans);
        fflush(stdin);
        if (ans == 'y') {
            uint64_t increment;
            printf("Enter an increment: ");
            scanf("%lu", &increment);
            fflush(stdin);
            fhIncreaseKey(heap, incNode, increment);
            findMinRes = fhFindMin(heap);
            printf("Element with minimum key is: %u.\n", findMinRes);
            incNode = heap->min;
        } else if (ans == 'n') {
            break;
        } else {
            fprintf(stderr, "WTF\n");
            exit(EXIT_FAILURE);
        }
    }
    // fhDecreaseKey test.
    for (;;) {
        printf("Node pointed: (key: %lu, elem: %u).\n", insertRes->key,
               insertRes->elem);
        printf("Want to decrease its key? [y/n] ");
        scanf("%c", &ans);
        fflush(stdin);
        if (ans == 'y') {
            uint64_t decrement;
            printf("Enter a decrement: ");
            scanf("%lu", &decrement);
            fflush(stdin);
            fhDecreaseKey(heap, insertRes, decrement);
            findMinRes = fhFindMin(heap);
            printf("Element with minimum key is: %u.\n", findMinRes);
        } else if (ans == 'n') {
            break;
        } else {
            fprintf(stderr, "WTF\n");
            exit(EXIT_FAILURE);
        }
    }
    // delete test.
    printf("Want to delete that node? [y/n] ");
    scanf("%c", &ans);
    fflush(stdin);
    if (ans == 'y') {
        FibTreeNode *deleted = fhDelete(heap, insertRes);
        printf("Deleted node: (key: %lu, elem: %u).\n", deleted->key,
                deleted->elem);
        eraseFibTreeNode(deleted);
        findMinRes = fhFindMin(heap);
        printf("Element with minimum key is: %u.\n", findMinRes);
    } else if ((ans != 'y') && (ans != 'n')) {
        fprintf(stderr, "WTF\n");
        exit(EXIT_FAILURE);
    }
    // Heap emptying test.
    printf("Emptying heap...\n");
    while (!isHeapEmpty(heap)) {
        FibTreeNode *curr = fhDeleteMin(heap);
        printf("Current minimum: (%lu, %u).\n", curr->key, curr->elem);
        eraseFibTreeNode(curr);
    }
    printf("Heap emptied!\n");
    performance:
    // PERFORMANCE TEST
    printf("Want to do the performance test? [y/n] ");
    scanf("%c", &ans);
    fflush(stdin);
    if (ans == 'n') {
        goto exit;
    } else if ((ans != 'y') && (ans != 'n')) {
        fprintf(stderr, "WTF\n");
        exit(EXIT_FAILURE);
    }
    srand(getpid());
    FibTreeNode **nodes = malloc(NODES * sizeof(FibTreeNode *));
    if (nodes == NULL) {
        fprintf(stderr, "ERROR: malloc failed.\n");
        exit(EXIT_FAILURE);
    }
    // insert test.
    printf("Doing %lu insertions...\n", NODES);
    uint64_t minKey = UINT64_MAX;
    ulong minIndex = 0;
    for (ulong i = 0; i < NODES; i++) {
        uint64_t newMin = rand() + 1;
        nodes[i] = fhInsert(heap, rand(), newMin);
        if (newMin < minKey) {
            minKey = newMin;
            minIndex = i;
        }
    }
    printf("Min. key inserted: %lu, min. key: %lu.\n", minKey, heap->min->key);
    // rebuild test.
    printf("Deleting the min. to trigger a rebuild...\n");
    eraseFibTreeNode(fhDeleteMin(heap));
    // Insert a new node to compensate for the loss in the array.
    nodes[minIndex] = fhInsert(heap, rand(), rand() + 1);
    printf("Have a look at your memory usage if you want!");
    scanf("%c", &ans);
    // fhIncreaseKey test.
    printf("Doing %lu key increments...\n", INCREASES);
    for (ulong i = 0; i < INCREASES; i++)
        fhIncreaseKey(heap, nodes[rand() % NODES], rand());
    // fhDecreaseKey test.
    printf("Doing %lu key decrements...\n", DECREASES);
    for (ulong i = 0; i < DECREASES; i++) {
        int node = rand() % NODES;
        uint64_t dec = rand() % nodes[node]->key;
        fhDecreaseKey(heap, nodes[node], dec);
    }
    // delete test.
    printf("Doing %lu node deletions...\n", DELETIONS);
    //FILE *outFile = fopen("out.txt", "w+");
    for (ulong i = 0; i < DELETIONS; i++) {
        eraseFibTreeNode(fhDelete(heap, nodes[i % NODES]));
        if (isHeapEmpty(heap)) break;
        /*FibTreeNode *deleted = fhDeleteMin(heap);
        fprintf(outFile, "%lu\n", deleted->key);
        eraseFibTreeNode(deleted);*/
    }
    free(nodes);
    //fclose(outFile);
    exit:
    // HEAP ERASE TEST
    printf("Erasing heap...\n");
    eraseFibHeap(heap);
    printf("Done!\n");
    exit(EXIT_SUCCESS);
}
