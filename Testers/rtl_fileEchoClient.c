/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 30/8/2019
 * -----------------------------------------------------------------------------
 * Tester application for the RTL library. Transfers a file in both ways
 * between a client and a server.
 * This client requires the FULL path to the file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "../librtl/rtl.h"

#include "addressing.h"

#if __BIG_ENDIAN__
#define htonll(x) (x)
#define ntohll(x) (x)
#else
#define htonll(x) ((uint64_t)htonl((x) & 0xFFFFFFFF) << 32) | htonl((x) >> 32)
#define ntohll(x) ((uint64_t)ntohl((x) & 0xFFFFFFFF) << 32) | ntohl((x) >> 32)
#endif

#define READ_BUFSIZE 4000000
#define WRITE_BUFSIZE 500000

#define fflush(stdin) while (getchar() != '\n')

int main(void) {
    char recvFile[10];
    pid_t pid = getpid();
    sprintf(recvFile, "%ld", pid);

    // Initialize buffers.
    char *buf = malloc(READ_BUFSIZE);
    char *buf2 = malloc(WRITE_BUFSIZE);
    // Get and open file.
    FILE *inFile = fopen("testFile", "r");
    if (inFile == NULL) {
        fprintf(stderr, "ERROR: Failed to open test file.\n");
        exit(EXIT_FAILURE);
    }
    // Ask for configuration values.
    unsigned int lossProb;
    unsigned int N;
    int T;
    printf("Enter loss probability [0, 99]: ");
    scanf("%u", &lossProb);
    fflush(stdin);
    printf("Enter window size: ");
    scanf("%u", &N);
    fflush(stdin);
    printf("Enter timeout (-1 for TCP-like): ");
    scanf("%d", &T);
    fflush(stdin);
    // Initialize RTL.
    int sockFD = rtlInit(NULL, 0);
    if (sockFD == RTL_ERROR) {
        fprintf(stderr, "ERROR: Failed to initialize RTL.\n");
        exit(EXIT_FAILURE);
    }
    // Connect to server.
    RTLConnectValues *connData = rtlConnect(SERVER_IP, SERVER_PORT, sockFD, N,
                                            T, (unsigned char)lossProb);
    if (connData == NULL) {
        fprintf(stderr, "ERROR: Failed to connect to server.\n");
        exit(EXIT_FAILURE);
    }
    printf("Connection to server established!\n");
    // Send file length.
    size_t fileLen = 0;
    struct stat fileStat;
    memset(&fileStat, 0, sizeof(struct stat));
    stat("testFile", &fileStat);
    fileLen = fileStat.st_size;
    printf("Going to send %lu bytes.\n", fileLen);
    uint64_t fileLen_net = htonll(fileLen);
    if (rtlSend(0, connData, (char *)&fileLen_net, 8) != 8) {
        fprintf(stderr, "ERROR: Failed to send file size.\n");
        exit(EXIT_FAILURE);
    }
    // Open new channel.
    int chan1FD = rtlOpenChannel(connData, 1);
    if (chan1FD == RTL_ERROR) {
        fprintf(stderr, "ERROR: Failed to open new channel.\n");
        exit(EXIT_FAILURE);
    }
    // Give the server time to open the channel and get ready.
    sleep(1);
    // Read and send file.
    size_t sentBytes = 0;
    ssize_t sendRes;
    size_t readRes;
    do {
        memset(buf, 0, READ_BUFSIZE);
        readRes = fread(buf, 1, READ_BUFSIZE, inFile);
        if (readRes <= 0) {
            fprintf(stderr, "ERROR: Failed to read from file.\n");
            exit(EXIT_FAILURE);
        }
        sendRes = rtlSend(1, connData, buf, readRes);
        if (sendRes != readRes) {
            fprintf(stderr, "ERROR: Read %lu bytes, delivered %lu.\n",
                    readRes, sendRes);
            exit(EXIT_FAILURE);
        }
        sentBytes += sendRes;
    } while (sentBytes < fileLen);
    fclose(inFile);
    printf("File sent!\n");
    // Read file back.
    FILE *outFile = fopen(recvFile, "w+");
    if (outFile == NULL) {
        fprintf(stderr, "ERROR: Failed to open new file.\n");
        exit(EXIT_FAILURE);
    }
    printf("File name: %s\n", recvFile);
    size_t recvBytes = 0;
    ssize_t recvRes;
    do {
        memset(buf2, 0, WRITE_BUFSIZE);
        recvRes = rtlRecv(1, chan1FD, NULL, buf2, WRITE_BUFSIZE);
        if (recvRes <= 0) {
            fprintf(stderr, "ERROR: rtlRecv failed.\n");
            exit(EXIT_FAILURE);
        }
        if (fwrite(buf2, 1, recvRes, outFile) != recvRes) {
            fprintf(stderr, "ERROR: Failed to write to file.\n");
            exit(EXIT_FAILURE);
        }
        recvBytes += recvRes;
    } while (recvBytes < fileLen);
    fclose(outFile);
    printf("File read back!\n");
    // Close channel.
    if (rtlCloseChannel(1, chan1FD, connData) < 0) {
        fprintf(stderr, "ERROR: Failed to close new channel.\n");
        exit(EXIT_FAILURE);
    }
    printf("Done!\n");
    // Close connection.
    if (rtlClose(connData) != 0) {
        fprintf(stderr, "ERROR: Failed to close connection.\n");
        exit(EXIT_FAILURE);
    }
    printf("Connection closed!\n");
    free(buf);
    free(buf2);
    exit(EXIT_SUCCESS);
}
