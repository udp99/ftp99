/* Roberto Masocco
 * 1/8/2019
 * -----------------------------------------------------------------------------
 * Little test program for the pthread cancellation family.
 * Two threads print strings pushed in the cancellation callback stack, then
 * exit.
 * No error checking is done for the sake of Jesus Christ it's late and I
 * need to (watch Chernobyl and) sleep.
 * ulongs are used to calm GCC down.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

void *worker(void *arg);
void cleanupRoutine(void *arg);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

/* The works. */
int main(void) {
    pthread_t tids[2];
    printf("Spawning children...\n");
    for (ulong i = 0; i < 2; i++)
        pthread_create(tids + i, NULL, worker, (void *)(i + 1));
    printf("Killing them in 2 seconds...\n");
    sleep(2);
    for (int i = 0; i < 2; i++) pthread_cancel(tids[i]);
    // Now, you shall the threads competing for the lock on stdout and
    // printing their three numbers before exiting.
    for (int i = 0; i < 2; i++) pthread_join(tids[i], NULL);
    printf("Test completed!\n");
    exit(EXIT_SUCCESS);
}

/* Cleanup routine. */
void cleanupRoutine(void *arg) {
    ulong num = (ulong)arg;
    printf("%lu\n", num);
}

/* Child thread routine. You can test both cancellation types. */
void *worker(void *arg) {
    ulong index = (ulong)arg;
    int prevCancShit;
    // Set thread cancellation behaviour.
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &prevCancShit);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &prevCancShit);
    //pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &prevCancShit);
    // Push cleanup routines in the callback stack.
    pthread_cleanup_push(cleanupRoutine, (void *)(index + 1));
    pthread_cleanup_push(cleanupRoutine, (void *)(index + 2));
    pthread_cleanup_push(cleanupRoutine, (void *)(index + 3));
    // Wait for cancellation.
    for (;;) sleep(1);
    /* What follows now is a complete load of bullshit. I don't know why, but
     * the code, if such "functions" are used, MUST be written like this in
     * order to work. So no, I couldn't use a "for" loop. All 3 calls by hand.
     * This is because those are macros that contain a "do {" in the "push"
     * and a "} while (0);" in the "pop". So, when they expand, they must both
     * be present AND at the same level.
     * From pthread.h itself:
     *
     * pthread_cleanup_push and pthread_cleanup_pop are macros and must always
     * be used in matching pairs at the same nesting level of braces.
     *
     * More upsetting shit from Ubuntu bug tracking site:
     *
     * Posix states that these
     * can be implemented as macros that might introduce opening and
     * closing braces, and that using setjmp/longjmp/return/break/continue
     * between them results in undefined behaviour.
     *
     * Moreover (from another example with a "return" where we have "sleep"):
     *
     * If we run the same program on FreeBSD or Mac OS X, we see that the
     * program incurs a segmentation violation and drops core. This happens
     * because on these systems, pthread_cleanup_push is implemented as a macro
     * that stores some context on the stack. When thread 1 returns in between
     * the call to pthread_cleanup_push and the call to pthread_cleanup_pop,
     * the stack is overwritten and these platforms try to use this
     * (now corrupted) context when they invoke the cleanup handlers. In the
     * Single UNIX Specification, returning while in between a matched pair of
     * calls to pthread_cleanup_push and pthread_cleanup_pop results in
     * undefined behavior. The only portable way to return in between these two
     * functions is to call pthread_exit.
     *
     * Now... HOW THE FUCK MUCH RETARDED DO YOU HAVE TO BE TO COME UP WITH AN
     * IMPLEMENTATION LIKE THIS???!!!
     * Even if those are unreachable code they have to be there, otherwise
     * this won't compile to shit, throwing all sorts of errors.
     * Pay attention. We'll have to use 'em like this.
     * The reason appears to be the fact that the whole "callback stack"
     * magic is made using "setjmp" and "longjmp", which are fancy gotos that
     * also restore the context, and that require some weirdly placed code in
     * order to be implemented.
     */
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
    pthread_cleanup_pop(0);
}

#pragma clang diagnostic pop
