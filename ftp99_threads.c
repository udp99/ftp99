/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 10/9/2019
 * -----------------------------------------------------------------------------
 * Worker threads of the whole FTP99 application.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <pthread.h>
#include <signal.h>

#include "librtl/rtl.h"

#include "Libraries/DoubleLinkedList/doubleLinkedList.h"

#include "FTP99.h"

#define __PUT_CHUNK 4000000
#define __GET_CHUNK 1500000

extern DLList *workers;
extern pthread_mutex_t *workersLock;

pthread_mutex_t consoleLock;

/* Thread cancellation routine. */
void workerCleanup(void *args) {
    __workerArgs *myArgs = (__workerArgs *)args;
    if (myArgs->newChanPipe != -1)
        rtlCloseChannel(myArgs->newChan, myArgs->newChanPipe, myArgs->connData);
    fsync(myArgs->reqFile);
    close(myArgs->reqFile);
    free(myArgs);
}

/* Worker thread to receive a file. */
void *getter(void *args) {
    __workerArgs *myArgs = (__workerArgs *)args;
    Record *myRecord = myArgs->thisRec;
    pthread_cleanup_push(workerCleanup, myArgs);
    pthread_detach(myArgs->workerTID);
    if (myArgs->isServer == 1)
        pthread_setname_np(myArgs->workerTID, "ftp99_sGetter");
    else pthread_setname_np(myArgs->workerTID, "ftp99_cGetter");
    // Receive the file.
    if (myArgs->isServer == 1)
        printf("Receiving file from client no. %lu...\n", myArgs->clNum);
    else {
        pthread_mutex_lock(&consoleLock);
        printf("Download of file \"%s\" starting...\n", myArgs->fileName);
        pthread_mutex_unlock(&consoleLock);
    }
    size_t recvBytes = 0;
    ssize_t recvRes;
    char recBuf[__GET_CHUNK];
    do {
        memset(recBuf, 0, __GET_CHUNK);
        recvRes = rtlRecv(myArgs->newChan, myArgs->newChanPipe,
                          myArgs->connData, recBuf, __GET_CHUNK);
        if (recvRes <= 0) {
            fprintf(stderr, "ERROR: I/O error during download.\n");
            perror("rtlRecv");
            kill(getpid(), SIGPIPE);
        }
        size_t wroteBytes = 0;
        ssize_t writeRes;
        do {
            writeRes = write(myArgs->reqFile, recBuf + wroteBytes,
                             recvRes - wroteBytes);
            if (writeRes < 0) {
                fprintf(stderr, "ERROR: I/O error while writing on file.\n");
                perror("write");
                kill(getpid(), SIGPIPE);
            }
            wroteBytes += writeRes;
        } while (wroteBytes < recvRes);
        recvBytes += recvRes;
    } while (recvBytes < myArgs->fileSize);
    if (myArgs->isServer == 1) {
        int oldState;
        ssize_t sendRes;
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldState);
        sendRes = rtlSend(myArgs->newChan, myArgs->connData, OKP, 3);
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldState);
        if (sendRes <= 0) {
            fprintf(stderr, "ERROR: Failed to send PUT positive reply.\n");
            perror("rtlSend");
            kill(getpid(), SIGPIPE);
        }
        printf("Download from client no. %lu done!\n", myArgs->clNum);
    } else {
        pthread_mutex_lock(&consoleLock);
        printf("Download of file \"%s\" done!\n", myArgs->fileName);
        pthread_mutex_unlock(&consoleLock);
    }
    // Terminate.
    pthread_mutex_lock(workersLock);  // To avoid deadlocks when closing.
    pthread_cleanup_pop(1);
    eraseRecord(popRecord(myRecord, workers));
    pthread_mutex_unlock(workersLock);
    pthread_exit(NULL);
}

/* Worker thread to send a file. */
void *putter(void *args) {
    __workerArgs *myArgs = (__workerArgs *)args;
    Record *myRecord = myArgs->thisRec;
    pthread_cleanup_push(workerCleanup, myArgs);
    pthread_detach(myArgs->workerTID);
    if (myArgs->isServer == 1)
        pthread_setname_np(myArgs->workerTID, "ftp99_sPutter");
    else pthread_setname_np(myArgs->workerTID, "ftp99_cPutter");
    // Map the file in memory for the sake of speed.
    char *reqFile = (char *)mmap(NULL, myArgs->fileSize, PROT_READ,
                                 MAP_PRIVATE, myArgs->reqFile, 0);
    if (reqFile == MAP_FAILED) {
        fprintf(stderr, "ERROR: Failed to map file in memory.\n");
        perror("mmap");
        kill(getpid(), SIGPIPE);
    }
    if (myArgs->isServer == 1)
        printf("Starting upload for client no. %lu...\n", myArgs->clNum);
    else {
        pthread_mutex_lock(&consoleLock);
        printf("Upload of file \"%s\" starting...\n", myArgs->fileName);
        pthread_mutex_unlock(&consoleLock);
    }
    // Send the file.
    int oldState;
    size_t sentBytes = 0, chunkLen;
    ssize_t sendRes;
    do {
        if ((myArgs->fileSize - sentBytes) < __PUT_CHUNK)
            chunkLen = myArgs->fileSize - sentBytes;
        else chunkLen = __PUT_CHUNK;
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &oldState);
        sendRes = rtlSend(myArgs->newChan, myArgs->connData,
                          reqFile + sentBytes, chunkLen);
        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldState);
        if (sendRes < 0) {
            fprintf(stderr, "ERROR: I/O error during upload.\n");
            perror("rtlSend");
            kill(getpid(), SIGPIPE);
        }
        sentBytes += sendRes;
    } while (sentBytes < myArgs->fileSize);
    // Unmap file from memory.
    if (munmap(reqFile, myArgs->fileSize) < 0) {
        fprintf(stderr, "ERROR: Failed to unmap file from memory.\n");
        perror("munmap");
        kill(getpid(), SIGPIPE);
    }
    if (myArgs->isServer == 1)
        printf("Upload for client no. %lu done!\n", myArgs->clNum);
    else {
        char okp[4] = {0x00, 0x00, 0x00, 0x00};
        if ((rtlRecv(myArgs->newChan, myArgs->newChanPipe, myArgs->connData,
                     okp, 3) <= 0) ||
            (strcmp(okp, OKP) != 0)) {
            fprintf(stderr, "ERROR: Didn't receive PUT positive reply.\n");
            perror("rtlRecv");
            kill(getpid(), SIGPIPE);
        }
        pthread_mutex_lock(&consoleLock);
        printf("Upload of file \"%s\" done!\n", myArgs->fileName);
        pthread_mutex_unlock(&consoleLock);
    }
    // Terminate.
    pthread_mutex_lock(workersLock);  // To avoid deadlocks when closing.
    pthread_cleanup_pop(1);
    eraseRecord(popRecord(myRecord, workers));
    pthread_mutex_unlock(workersLock);
    pthread_exit(NULL);
}
