/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 5/9/2019
 * -----------------------------------------------------------------------------
 * Worker threads of the FTP99 server application.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include "../Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h"
#include "../Libraries/DoubleLinkedList/doubleLinkedList.h"

#include "../librtl/rtl.h"

#define UNUSED(arg) (void)arg

extern AVLIntTree *clientProcs;
extern pthread_mutex_t clientsLock;

extern RTLConnectValues *clientConn;
extern DLList *workers;
extern pthread_mutex_t *workersLock;

/* Signal handler for SIGCHLD (reaper thread). */
void childCloseHandler(int sig, siginfo_t *info, void *ucontext);

/* Threads cancellation routines. */
void reaperCleanup(void *args);

/* Keeps an eye on terminating clients processes and updates their index. */
void *clientsReaper(void *args) {
    UNUSED(args);
    pthread_cleanup_push(reaperCleanup, NULL);
    // Set this thread's signal mask and SIGCHLD handler.
    sigset_t reaperSet, ignSet;
    struct sigaction chldAct;
    memset(&chldAct, 0, sizeof(struct sigaction));
    sigfillset(&reaperSet); sigfillset(&ignSet);
    sigdelset(&reaperSet, SIGCHLD); sigdelset(&reaperSet, SIGKILL);
    sigdelset(&reaperSet, SIGSTOP); sigdelset(&reaperSet, SIGABRT);
    chldAct.sa_sigaction = childCloseHandler;
    chldAct.sa_flags = SA_SIGINFO;
    chldAct.sa_mask = ignSet;
    if ((pthread_sigmask(SIG_SETMASK, &reaperSet, NULL) != 0) ||
        (sigaction(SIGCHLD, &chldAct, NULL) != 0)) {
        fprintf(stderr, "ERROR: Reaper thread failed to configure signals.\n");
        perror("pthread_sigmask/sigaction");
        pthread_mutex_destroy(&clientsLock);
        deleteIntTree(clientProcs);
        exit(EXIT_FAILURE);
    }
    // Name yourself.
    pthread_setname_np(pthread_self(), "ftp99_sReaper");
    for (;;)
        // Wait for children to terminate and remove their info.
        pause();
    pthread_cleanup_pop(0);
}
