/* Francesco Mancini
 * Alessandro Picco
 * Roberto Masocco
 * 5/9/2019
 * -----------------------------------------------------------------------------
 * Main source file for the FTP99 server application.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sched.h>
#include <pthread.h>
#include <signal.h>
#include <netinet/in.h>
#include <bits/sigaction.h>

#include "../Libraries/AVLTrees_IntegerKeys/AVLTree_IntegerKeys.h"
#include "../Libraries/DoubleLinkedList/doubleLinkedList.h"

#include "../librtl/rtl.h"

#include "../FTP99.h"

#define flush(stdin) while (getchar() != '\n')

/* Index of all connected clients processes. */
AVLIntTree *clientProcs = NULL;
pthread_mutex_t clientsLock;

/* Client connection data (for child processes). */
RTLConnectValues *clientConn = NULL;

/* Worker threads data (for child processes). */
DLList *workers = NULL;
pthread_mutex_t *workersLock = NULL;

/* Client number (for child processes). */
ulong clientNum = 0;

/* Worker threads. */
void *getter(void *args);
void *putter(void *args);
int lister(void *pipePtr);

/* Socket to accept connections on. */
int accSock;

/* Child process routine. */
void clientWorker(RTLAcceptValues *newConn, ulong clNum);

/* Child processes reaper thread. */
pthread_t reaperTID;
void *clientsReaper(void *args);

/* Worker threads for client operations. */

/* Signal handler to initiate closing routines. */
void mainTermHandler(int sig);
void childTermHandler(int sig);

/* The works. */
int main(int argc, char **argv) {
    // Name yourself.
    memset(argv[0], 0, strlen(argv[0]));
    strcpy(argv[0], "ftp99_sMain");
    pthread_setname_np(pthread_self(), "ftp99_sMain");
    char ipAns = 0;
    // Check input arguments: root path, IP, port.
    char srvIP[INET_ADDRSTRLEN];
    memset(srvIP, 0, INET_ADDRSTRLEN);
    uint16_t srvPort = 0;
    if (argc < 2) {
        fprintf(stderr, "Usage: ftp99_server ROOT [IP] [PORT]\n"
                        "\tROOT: Absolute path to server's working directory"
                        " (must have read/write permissions on this).\n"
                        "\tIP: Server's IP address (of the interface to bind "
                        "to) (optional).\n"
                        "\tPORT: Server port (may be specified manually).\n");
        exit(EXIT_FAILURE);
    }
    if (argc != 4) {
        // Ask the user for other configuration parameters.
        printf("Do you want to use a specific IP address? [y/n] ");
        scanf("%c", &ipAns);
        flush(stdin);
        if (ipAns == 'y') {
            printf("Enter the IP address to use: ");
            if (scanf("%s", srvIP) != 1) {
                fprintf(stderr, "ERROR: Invalid input.\n");
                exit(EXIT_FAILURE);
            }
            flush(stdin);
        }
        printf("Enter the port to use for this server: ");
        if (scanf("%hu", &srvPort) != 1) {
            fprintf(stderr, "ERROR: Invalid input.\n");
            exit(EXIT_FAILURE);
        }
        flush(stdin);
    } else {
        // Get configuration parameters from argv.
        memcpy(srvIP, argv[2], INET_ADDRSTRLEN);
        sscanf(argv[3], "%hu", &srvPort);
    }
    // Set process working directory to server's data root.
    if ((chdir(argv[1]) != 0) ||
        (setenv("PWD", argv[1], 1) != 0)) {
        fprintf(stderr, "ERROR: Failed to set working directory.\n");
        perror("chdir/setenv");
        exit(EXIT_FAILURE);
    }
    // Configure signal behaviour.
    sigset_t ignSet, mainSet;
    sigfillset(&ignSet);
    sigfillset(&mainSet);
    sigdelset(&mainSet, SIGINT); sigdelset(&mainSet, SIGTERM);
    sigdelset(&mainSet, SIGPIPE); sigdelset(&mainSet, SIGSEGV);
    sigdelset(&mainSet, SIGBUS); sigdelset(&mainSet, SIGSTOP);
    sigdelset(&mainSet, SIGKILL); sigdelset(&mainSet, SIGABRT);
    sigdelset(&mainSet, SIGQUIT);
    if (pthread_sigmask(SIG_SETMASK, &mainSet, NULL) != 0) {
        fprintf(stderr, "ERROR: Failed to set main thread signal mask.\n");
        perror("pthread_sigmask");
        exit(EXIT_FAILURE);
    }
    struct sigaction mainTermAct;
    memset(&mainTermAct, 0, sizeof(struct sigaction));
    mainTermAct.sa_handler = mainTermHandler;
    mainTermAct.sa_mask = ignSet;
    if ((sigaction(SIGINT, &mainTermAct, NULL) != 0) ||
        (sigaction(SIGTERM, &mainTermAct, NULL) != 0) ||
        (sigaction(SIGPIPE, &mainTermAct, NULL) != 0)) {
        fprintf(stderr, "ERROR: Failed to set signal handlers in main thread"
                        ".\n");
        perror("sigaction");
        exit(EXIT_FAILURE);
    }
    // Initialize clients processes list.
    clientProcs = createIntTree();
    if (clientProcs == NULL) {
        fprintf(stderr, "ERROR: Failed to create clients processes list.\n");
        exit(EXIT_FAILURE);
    }
    pthread_mutex_init(&clientsLock, NULL);
    // Spawn clients reaper thread.
    if (pthread_create(&reaperTID, NULL, clientsReaper, NULL) != 0) {
        fprintf(stderr, "ERROR: Failed to spawn reaper thread.\n");
        deleteIntTree(clientProcs);
        pthread_mutex_destroy(&clientsLock);
        exit(EXIT_FAILURE);
    }
    printf("FTP99 Server coming up...\n");
    // Initialize RTL.
    if (ipAns == 'n') accSock = rtlInit("", srvPort);
    else accSock = rtlInit(srvIP, srvPort);
    if (accSock == RTL_ERROR) {
        fprintf(stderr, "ERROR: Failed to initialize RTL.\n");
        perror("rtlInit");
        deleteIntTree(clientProcs);
        pthread_mutex_destroy(&clientsLock);
        exit(EXIT_FAILURE);
    }
    printf("FTP99 Server ready!\n");
    // FTP99 Server operations loop.
    ulong clientsCount = 0;
    for (;;) {
        // Accept a new connection.
        RTLAcceptValues *newConn = rtlAccept(accSock);
        if (newConn == NULL) {
            fprintf(stderr, "ERROR: Failed to accept a new connection.\n");
            perror("rtlAccept");
            raise(SIGTERM);
        }
        printf("New connection!\n");
        // Block ALL signals to avoid deadlocks.
        pthread_sigmask(SIG_SETMASK, &ignSet, NULL);
        pthread_mutex_lock(&clientsLock);
        if (clientProcs->nodesCount == CLIENTS_MAX) {
            fprintf(stderr, "ERROR: Server full, dropping client.\n");
            pthread_mutex_unlock(&clientsLock);
            pthread_sigmask(SIG_SETMASK, &mainSet, NULL);
            free(newConn);
            continue;
        }
        // Generate the new client worker process.
        pid_t newClient = fork();
        if (newClient == 0) {
            // Child process: destroy copies of parent's data and start work
            // routine.
            strcpy(argv[0], "ftp99_sWorker");  // Name yourself.
            pthread_sigmask(SIG_SETMASK, &mainSet, NULL);
            pthread_mutex_destroy(&clientsLock);
            deleteIntTree(clientProcs);
            clientWorker(newConn, clientsCount);
        } else if (newClient == -1) {
            fprintf(stderr, "ERROR: Failed to spawn new client worker.\n");
            perror("fork");
            pthread_mutex_unlock(&clientsLock);
            pthread_sigmask(SIG_SETMASK, &mainSet, NULL);
            raise(SIGTERM);
        } else {
            // Parent process: add PID to list.
            intInsert(clientProcs, (int)newClient, 0);
            pthread_mutex_unlock(&clientsLock);
            // Restore signals behaviour.
            pthread_sigmask(SIG_SETMASK, &mainSet, NULL);
            free(newConn);  // Unneeded in parent process.
        }
        clientsCount++;
    }
    return 0;
}

/* Client worker routine. */
void clientWorker(RTLAcceptValues *newConn, ulong clNum) {
    // Block SIGINT/SIGTERM, unlock and set handler for SIGPIPE/SIGUSR1.
    sigset_t childSet, ignSet, ignSet2;
    sigemptyset(&childSet);
    sigfillset(&ignSet); sigdelset(&ignSet, SIGPIPE);
    sigfillset(&ignSet2);
    if (pthread_sigmask(0, NULL, &childSet) != 0) {
        fprintf(stderr, "ERROR: Child failed to get signal mask.\n");
        perror("pthread_sigmask");
        free(newConn);
        close(accSock);
        exit(EXIT_FAILURE);
    }
    sigaddset(&childSet, SIGINT); sigaddset(&childSet, SIGTERM);
    sigdelset(&childSet, SIGUSR1);
    if (pthread_sigmask(SIG_SETMASK, &childSet, NULL) != 0) {
        fprintf(stderr, "ERROR: Child failed to set signal mask.\n");
        perror("pthread_sigmask");
        free(newConn);
        close(accSock);
        exit(EXIT_FAILURE);
    }
    struct sigaction childTermAct, childConnErrAct;
    memset(&childTermAct, 0, sizeof(struct sigaction));
    memset(&childConnErrAct, 0, sizeof(struct sigaction));
    childTermAct.sa_handler = childTermHandler;
    childTermAct.sa_mask = ignSet;
    childTermAct.sa_flags = 0;
    childConnErrAct.sa_handler = childTermHandler;
    childConnErrAct.sa_mask = ignSet2;
    childConnErrAct.sa_flags = 0;
    if ((sigaction(SIGUSR1, &childTermAct, NULL) != 0) ||
        (sigaction(SIGPIPE, &childConnErrAct, NULL) != 0)) {
        fprintf(stderr, "ERROR: Child failed to set signal handlers.\n");
        perror("sigaction");
        free(newConn);
        close(accSock);
        exit(EXIT_FAILURE);
    }
    // Create workers metadata.
    workersLock = calloc(1, sizeof(pthread_mutex_t));
    workers = createDLList();
    if ((workersLock == NULL) || (workers == NULL)) {
        fprintf(stderr, "ERROR: Child failed to create workers metadata.\n");
        free(newConn);
        close(accSock);
        exit(EXIT_FAILURE);
    }
    pthread_mutex_init(workersLock, NULL);
    // Attach to new client.
    clientConn = rtlAttach(newConn, accSock, 1);
    if (clientConn == NULL) {
        fprintf(stderr, "ERROR: Child failed to attach new connection.\n");
        perror("rtlAttach");
        free(newConn);
        close(accSock);
        exit(EXIT_FAILURE);
    }
    clientNum = clNum;
    printf("Client no. %lu connected!\n", clNum);
    // Name yourself (pt. 2).
    pthread_setname_np(pthread_self(), "fpt99_sWorker");
    // FTP99 client process operations loop.
    char newCmd[CMD_MAXLEN + 1];
    for (;;) {
        // Receive a new command from channel 0.
        memset(newCmd, 0, CMD_MAXLEN + 1);
        if (rtlRecv(0, 0, clientConn, newCmd, CMD_MAXLEN) <= 0) {
            fprintf(stderr, "ERROR: Failed to receive new command from client"
                            " no. %lu.\n", clNum);
            perror("rtlRecv");
            raise(SIGPIPE);
        }
        // Understand what the fuck the client wants.
        char cmdHdr[HDR_LEN + 1];
        memcpy(cmdHdr, newCmd, HDR_LEN);
        cmdHdr[HDR_LEN] = '\0';
        if (strcmp(cmdHdr, GET) == 0) {
            // Client wants to download a file.
            sigset_t oldSet;
            pthread_sigmask(SIG_SETMASK, &ignSet2, &oldSet);
            __workerArgs *opArgs = calloc(1, sizeof(__workerArgs));
            if (opArgs == NULL) {
                fprintf(stderr, "ERROR: Failed to get memory for argument buffer"
                                ".\n");
                char errMsg[] = ERR "Server memory error.";
                rtlSend(0, clientConn, errMsg, 24);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
                return;
            }
            printf("Client no. %lu requested a GET for file \"%s\".\n",
                   clNum, newCmd + HDR_LEN + 1);
            opArgs->isServer = 1;
            opArgs->newChan = (int)newCmd[HDR_LEN];
            opArgs->clNum = clNum;
            opArgs->connData = clientConn;
            opArgs->newChanPipe = -1;
            struct stat reqFileStat;
            memset(&reqFileStat, 0, sizeof(struct stat));
            if (((opArgs->reqFile = open(newCmd + HDR_LEN + 1, O_RDONLY))
                < 0) ||
                (fstat(opArgs->reqFile, &reqFileStat) != 0)) {
                fprintf(stderr, "ERROR: Failed to open requested file.\n");
                perror("open/fstat");
                free(opArgs);
                char errMsg[] = ERR "File not found.";
                rtlSend(0, clientConn, errMsg, 19);
                continue;
            }
            opArgs->fileSize = reqFileStat.st_size;
            if (pthread_create(&(opArgs->workerTID), NULL,
                                putter, opArgs) != 0) {
                fprintf(stderr, "ERROR: Failed to spawn worker thread.\n");
                free(opArgs);
                char errMsg[] = ERR "Server fatal error.";
                rtlSend(0, clientConn, errMsg, 23);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
            }
            pthread_mutex_lock(workersLock);
            Record *newWorker = addAsLast(opArgs, workers);
            pthread_mutex_unlock(workersLock);
            if (newWorker == NULL) {
                fprintf(stderr, "ERROR: Failed to manage workers metadata.\n");
                char errMsg[] = ERR "Server memory error.";
                rtlSend(0, clientConn, errMsg, 24);
                opArgs->thisRec = NULL;
                pthread_cancel(opArgs->workerTID);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
            }
            opArgs->thisRec = newWorker;
            // Send operation ACK to client (with file size).
            uint64_t fileSize = reqFileStat.st_size;
            fileSize = htonll(fileSize);
            char okg[11];
            memset(okg, 0, sizeof okg);
            memcpy(okg, OKG, 3);
            memcpy(okg + 3, &fileSize, 8);
            rtlSend(0, clientConn, okg, 11);
            pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
        } else if (strcmp(cmdHdr, PUT) == 0) {
            // Client wants to send a file.
            sigset_t oldSet;
            pthread_sigmask(SIG_SETMASK, &ignSet2, &oldSet);
            __workerArgs *opArgs = calloc(1, sizeof(__workerArgs));
            if (opArgs == NULL) {
                fprintf(stderr, "ERROR: Failed to get memory for argument buffer"
                                ".\n");
                char errMsg[] = ERR "Server memory error.";
                rtlSend(0, clientConn, errMsg, 24);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
                return;
            }
            opArgs->isServer = 1;
            opArgs->newChan = (int)newCmd[HDR_LEN];
            opArgs->clNum = clNum;
            opArgs->connData = clientConn;
            uint64_t fileSize = 0;
            memcpy(&fileSize, newCmd + HDR_LEN + 1, 8);
            fileSize = ntohll(fileSize);
            opArgs->fileSize = (size_t)fileSize;
            printf("Client no. %lu requested a PUT for file \"%s\" of %lu bytes"
                   ".\n",
                   clNum, newCmd + HDR_LEN + 1 + FILESZ_LEN, fileSize);
            if ((opArgs->newChanPipe = rtlOpenChannel(clientConn,
                                                      opArgs->newChan)) <= 0) {
                fprintf(stderr, "ERROR: Failed to open new channel.\n");
                perror("rtlOpenChannel");
                free(opArgs);
                char errMsg[] = ERR "Server failed to open channel.";
                rtlSend(0, clientConn, errMsg, 34);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
            }
            if ((opArgs->reqFile = open(newCmd + HDR_LEN + 1 + FILESZ_LEN,
                                        O_CREAT | O_TRUNC | O_WRONLY,
                                        S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP))
                < 0) {
                rtlCloseChannel(opArgs->newChan, opArgs->newChanPipe,
                                clientConn);
                free(opArgs);
                char errMsg[] = ERR "Server failed to create file.";
                rtlSend(0, clientConn, errMsg, 33);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
            }
            if (pthread_create(&(opArgs->workerTID), NULL,
                               getter, opArgs) != 0) {
                fprintf(stderr, "ERROR: Failed to spawn worker thread.\n");
                rtlCloseChannel(opArgs->newChan, opArgs->newChanPipe,
                                clientConn);
                close(opArgs->reqFile);
                free(opArgs);
                char errMsg[] = ERR "Server fatal error.";
                rtlSend(0, clientConn, errMsg, 23);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
            }
            pthread_mutex_lock(workersLock);
            Record *newWorker = addAsLast(opArgs, workers);
            pthread_mutex_unlock(workersLock);
            if (newWorker == NULL) {
                fprintf(stderr, "ERROR: Failed to manage workers metadata.\n");
                char errMsg[] = ERR "Server memory error.";
                rtlSend(0, clientConn, errMsg, 24);
                opArgs->thisRec = NULL;
                pthread_cancel(opArgs->workerTID);
                pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
                raise(SIGPIPE);
            }
            opArgs->thisRec = newWorker;
            // Send operation ACK to client.
            rtlSend(0, clientConn, OKP, 3);
            pthread_sigmask(SIG_SETMASK, &oldSet, NULL);
        } else if (strcmp(cmdHdr, LST) == 0) {
            // Client requested a directory listing.
            printf("Client no. %lu requested a directory listing.\n", clNum);
            char lsBuf[LS_BUFSIZE + 1];
            memset(lsBuf, 0, LS_BUFSIZE + 1);
            // Prepare for a clone call.
            int lsPipe[2];
            void *lsStack = malloc(__LS_STACKSIZE);
            if ((lsStack == NULL) || (pipe(lsPipe) != 0)) {
                fprintf(stderr, "ERROR: Failed to prepare for directory "
                                "listing.\n");
                perror("malloc/pipe");
                char errMsg[] = ERR "Server fatal error.";
                rtlSend(0, clientConn, errMsg, 23);
                free(lsStack);
                raise(SIGPIPE);
            }
            pid_t lsPID;
            lsStack += __LS_STACKSIZE;  // Stack grows downward on
                                        // non-retarded ISAs.
            // Spawn a very lightweight new process which will transform into
            // ls.
            if (clone(lister, lsStack,
                      SIGCHLD | CLONE_IO | CLONE_PARENT_SETTID | CLONE_VM,
                      lsPipe + 1, &lsPID, NULL, NULL) == -1) {
                fprintf(stderr, "ERROR: Failed to clone for ls.\n");
                perror("clone");
                char errMsg[] = ERR "Server fatal error.";
                rtlSend(0, clientConn, errMsg, 23);
                free(lsStack);
                raise(SIGPIPE);
            }
            // In the parent process, read and transmit ls output.
            close(lsPipe[1]);
            while (read(lsPipe[0], lsBuf, LS_BUFSIZE)) {
                if (rtlSend(0, clientConn, lsBuf, strlen(lsBuf)) <= 0) {
                    fprintf(stderr, "ERROR: Failed to send ls data.\n");
                    perror("rtlSend");
                    kill(lsPID, SIGKILL);
                    free(lsStack);
                    raise(SIGPIPE);
                }
                memset(lsBuf, 0, LS_BUFSIZE + 1);
            }
            close(lsPipe[0]);
            if (rtlSend(0, clientConn, "\r\n", 3) <= 0) {
                fprintf(stderr, "ERROR: Failed to send ls data.\n");
                perror("rtlSend");
                raise(SIGPIPE);
            }
            if (waitpid(lsPID, NULL, 0) != lsPID) {
                fprintf(stderr, "ERROR: Failed to wait for ls.\n");
                perror("waitpid");
            } else
                printf("Directory listing for client no. %lu done.\n", clNum);
        } else if (strcmp(cmdHdr, CLS) == 0) {
            // Client requested disconnection.
            printf("Client no. %lu requested connection close.\n", clNum);
            rtlSend(0, clientConn, CLS, 3);
            pthread_sigmask(SIG_SETMASK, &ignSet2, NULL);  // Avoid deadlocks.
            pthread_mutex_lock(workersLock);
            if (workers->recsCount > 0) {
                Record *worker = workers->first;
                while (worker != NULL) {
                    pthread_t workerTID = ((__workerArgs *)(worker->recData))
                                          ->workerTID;
                    pthread_cancel(workerTID);
                    worker = worker->next;
                    popFirst(workers);
                }
            }
            pthread_mutex_destroy(workersLock);
            eraseList(workers);
            rtlClose(clientConn);
            printf("Connection with client no. %lu closed!\n", clNum);
            exit(EXIT_SUCCESS);
        } else {
            // Unknown command.
            fprintf(stderr, "ERROR: Unknown command from client no. %lu.\n",
                    clNum);
            char errMsg[] = ERR "Unknown command.";
            rtlSend(0, clientConn, errMsg, 20);
        }
    }
}
